# Contribute Documentation

This documentation is built using [Material for
MkDocs](https://squidfunk.github.io/mkdocs-material/). It provides a simple
developer-oriented workflow with the familiar Markdown language for writing.

Files can conveniently be edited via Gitea and submitted for review. Or a
[local version of the full documentation can be built](build_instructions.md)
for offline editing with your personal editor/tools.

## Getting Started

This documentation can be edited in two ways:

- Online, using the Gitea UI on [projects.blender.org](https://projects.blender.org/blender/blender-developer-docs)
- Offline, by [building a local version](build_instructions.md) of the documentation.

