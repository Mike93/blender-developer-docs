# Chat

[blender.chat](https://blender.chat) is the platform where many Blender
developers and users hang out. See the [channel
listing](https://blender.chat/directory) for all available channels. The
most popular channels are listed below.

>? INFO: **Issues with blender.chat?**
>
> If you experience issues on blender.chat itself go to the
> [\#blender-chat-meta](https://blender.chat/channel/blender-chat-meta) channel for help.

## Developer Channels
Developer channels are where developers and other contributors discuss ongoing
development. Please stay on topic in these channels. For user feedback, help and feature requests visit the [user-channels](#user-channels).

### General Development

**[\#blender-coders](https://blender.chat/channel/blender-coders)**:
Official Blender development chat room for discussions about Blender
core development, where most Blender developers hang out.

**[\#docs](https://blender.chat/channel/docs)**: Official Blender
documentation chat room where discussions about the Blender manual.

**[\#python](https://blender.chat/channel/python)**: If you need help
regarding Blender's Python functionality, this is the place to visit.

**[\#translations](https://blender.chat/channel/translations)**:
Official Blender channel for discussing the translation of the Blender
user interface and manual.

**[\#blender-builds](https://blender.chat/channel/blender-builds)**:
Need help compiling the Blender source code? Although it's quick and
easy for some people, not everyone is as lucky getting Blender to build
on their platform. Drop by if you need assistance.

### Modules

Module channels are where developers and other contribute discuss ongoing
development.

**[\#animation-module](https://blender.chat/channel/animation-module)**:
Animation development

**[\#compositor-module](https://blender.chat/channel/compositor-module)**: Compositor development, as part of the VFX & Video module

**[\#core-module](https://blender.chat/channel/core-module)**: Core
module development

**[\#eevee-viewport-module](https://blender.chat/channel/eevee-viewport-module)**:
EEVEE & Viewport development

**[\#grease-pencil-module](https://blender.chat/channel/grease-pencil-module)**:
Grease Pencil development

**[\#nodes-physics-module](https://blender.chat/channel/nodes-physics-module)**:
Nodes & Physics development

**[\#pipeline-assets-io-module](https://blender.chat/channel/pipeline-assets-io-module)**:
Pipeline, Assets & IO development

**[\#render-cycles-module](https://blender.chat/channel/render-cycles-module)**:
Render & Cycles development

**[\#sequencer-module](https://blender.chat/channel/sequencer-module)**:
Video Sequence Editor development, as part of the VFX & Video module

**[\#sculpt-paint-texture-module](https://blender.chat/channel/sculpt-paint-texture-module)**:
Sculpt, Paint and Texture development

**[\#module-triaging](https://blender.chat/channel/module-triaging)**:
Triaging team coordination (bugs and pull requests)

**[\#user-interface-module](https://blender.chat/channel/user-interface-module)**:
User Interface development

## User Channels

### Artists and Users

**[\#support](https://blender.chat/channel/support)**: General support
channel for those seeking help and advice with Blender, in a family
friendly moderated environment.

**[\#general](https://blender.chat/channel/general)**: Where
Blenderheads meet and chat, ask questions, or just have fun!

**[\#gameengines-armoury-godot-up-bge-panda-etc](https://blender.chat/channel/gameengines-armoury-godot-up-bge-panda-etc)**:
Blender no longer has an integrated game engine. But here you can find
discussions about the game engines, and their integrations.

### Feedback

Some modules have channels for user feedback and feature requests.

**[\#everything-nodes](https://blender.chat/channel/everything-nodes)**:
Nodes & Physics feedback

**[\#user-interface](https://blender.chat/channel/user-interface-module)**:
User Interface feedback

## Rules
Please follow the [code of conduct](code_of_conduct.md).

* Be patient if you don't get a reply immediately, people are not at their computer all day.
* Do not cross post the same message into multiple channels. We're a pretty small community, the same people will see your question regardless of where you post.
* Stay on topic. There are dedicated rooms for developers and for artists (see above).






