# Blender 3.5.1

Released on April 25th 2023, Blender 3.5.1 features the following bug
fixes:

- Active/default UV map legacy conversion with name conflict
  blender/blender#106584
- Add support for OpenPGL 0.5.0
  blender/blender@85c7cc898d
- Add-ons: X3D & BVH import error with Python 3.11
  blender/blender-addons@8038d3c3b059cf38f0574e9e5bb6d6b4b0bf92fe
- bpy.types.Text (region_as_string/region_from_string) crash
  blender/blender#107261
- Color picker broken with Wayland & AMD GPU
  blender/blender#106264
- Crash when canceling Sky Resize with mesh symmetry
  blender/blender#107020
- Crash when loading files with custom node groups
  blender/blender#106467
- Crash when OpenEXR IO fails
  blender/blender#106977
- Crash with muted
  nodeblender/blender#106982
- Cycles importance sampling with multiple suns works poorly
  blender/blender#106293
- Cycles multi GPU crash with vertex color baking
  blender/blender#106405
- Cycles shadow caustics not working with area lights
  blender/blender#107004
- EEVEE Render: World lighting does not affect volumetrics
  blender/blender#106440
- Entering Grease Pencil Vertex Paint mode crashes
  blender/blender#107125
- Fireflies with Nishita sky sun sampling at certain angles
  blender/blender#106706
- Fix `source_archive` ignoring addons
  blender/blender@dd3aaa3dd0
- Fix buffer overflow in BLI_path_frame_strip with long extensions
  blender/blender@56b9df86f8
- Fix Crash calling asset_generate_preview() in backgound mode
  blender/blender#105325
- Fix CUdeviceptr and hipDeviceptr_t build error on ppc64le architecture
  blender/blender@98a999a811
- Fix missing assets in the source archive
  blender/blender@8f3faae18b
- Fix Snap package error on startup in older Linux version
  blender/blender@4f2ed42a18
- Fix unnecessary edge pan updates
  blender/blender@ce2de91510
- Fix: Iteration for BMLayerCollection was broken
  blender/blender@9a5a3da2b0
- Fix: Metal null buffer initialization
  blender/blender#106807
- Fix: Remove unsupported data types in extrude and split edges nodes
  blender/blender#106926
- Fix: Show 'Exit group' menu entry also for non group nodes.
  blender/blender#106643
- Fix: Transform geometry node doesn't translate volumes correctly
  blender/blender@9e5f1d06cb
- Fix: VSE channel buttons invisible in Light theme
  blender/blender#107113
- GPencil Paste stroke duplicates to the next selected
  blender/blender#106590
- Handle exceptions in add fur operator
  blender/blender#106366
- ImageEngine: Improve Performance and Quality.
  blender/blender#106092
- Inconsistent display of active filters for import/export file dialogs
  blender/blender#90159
- Incorrect modifier deform evaluation result
  blender/blender#106802
- Index the right UVmap in BMesh
  blender/blender#106430
- Intel iGPU Crashes When Switching to Eevee
  blender/blender#106278
- MacOS/OpenGL doesn't draw anything Eevee related.
  blender/blender#106672
- Metal: Resolve high memory pressure on EEVEE render
  blender/blender#107221
- Missing xray check in snapping
  blender/blender#106478
- Motion tracking data lost on recovering autosave
  blender/blender#106722
- Motion triangles could have unnormalized normals
  blender/blender#106394
- Moving frame node jittering while cursor is still
  blender/blender#106043
- Overlay: Resolve motion path rendering in Metal
  blender/blender#106568
- Pose library does not autokey mirrored poses
  blender/blender#106856
- Pose library: fix context menu
  blender/blender-addons@d4d32b373158a3614a6d4546ebb684fbb73b4d27
- Properly clear CD_FLAG_ACTIVE/DEFAULT_COLOR flags
  blender/blender#107067
- Resolve box selection issue in Metal
  blender/blender#105450
- Resolve flashing Metal viewport
  blender/blender#106704
- Resolve Metal grease pencil fill
  blender/blender#106773
- Resolve Metal workload dependency
  blender/blender#106431
- Resolve texture paint selection in Metal
  blender/blender#106103
- Selection of bones in grease pencil weightpaint mode
  failsblender/blender#106998
- Selection offset in timeline when NLA track is offset
  blender/blender#106771
- Sharp edge attribute removed when all edges are sharp
  blender/blender#105926
- "Shift to extend" doesn't work in 3D View Collections panel
  blender/blender#106251
- Snap curves to surface operator does not update geometry
  blender/blender#106094
- Subdivision surface crash with more than 8 UV maps
  blender/blender#106745
- Texture paint removes evaluated mesh attributes
  blender/blender#105912
- Use correct function to get active uv layer
  blender/blender#106628
- UV stitch crash with hidden faces
  blender/blender#106396
