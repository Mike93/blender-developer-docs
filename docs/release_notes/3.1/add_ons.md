# Blender 3.1: Add-ons

## Copy Global Transform

The Copy Global Transform add-on makes it possible to copy the
world-space transform of the active object/bone, and paste it onto any
object/bone. This could be the same one at a different point in time, or
you can copy the FK control bone's global transform to the IK control
bone. Or to some object. If you want to have it locked to that
particular world transform for a number of frames, you can paste to
selected keyframes ("smart bake") or even let it create new frames (on
1s, 2s, etc.). The add-on will do all the counter-animation required to
keep the paste-target at the same global transform.

<File:blender-release-notes-31-copy-global-transform-panel.png%7CGlobal>
Transform panel, in the Animation tab of the 3D Viewport.
<File:blender-release-notes-31-copy-global-transform-baking.png%7CResult>
of "Paste and Bake": a range of keys was created to counter-animate the
parent, ensuring that the world transform remains the same.

## Rigify

- The `super_finger` rig can now place the optional IK control on a
  different layer
  (blender/blender-addons@9030e2c6d1a).
- The advanced generate settings panel has been overhauled, removing the
  overwrite/new toggle and name field, but adding a reference to the
  widget collection
  (blender/blender-addons@ece39d809c).
- Limbs can now be uniformly scaled using the gear shaped bone at their
  base
  (blender/blender-addons@2f1c38fd507).
- Limb FK controls now use the Aligned inherit scale mode
  (blender/blender-addons@3fc46407617).
- The leg rig can now generate two separate toe controls for IK and FK,
  which is necessary for 100% accurate snapping. This is enabled by
  default in metarigs.
  (blender/blender-addons@0391f865e12d)

## FBX I/O

- Improved export speed of 'rested' armatures
  (blender/blender-addons@1b0254b5315b).

## OBJ I/O

- Fix for [\#94516](http://developer.blender.org/T94516) brought back
  roughness handling into expected MTL behavior, but therefore
  introduces a value shifting of this roughness parameter when
  re-importing older OBJ files exported by Blender
  (blender/blender-addons@f26299bacc1).
- OBJ exporter is now implemented in C++, see [the Pipeline, Assets &
  I/O
  page](pipeline_assets_io.md#obj-i/o)
  for details.

## Atomic Blender (PDB/XYZ)

**PDB/XYZ importer**

- Fix for [\#94008](http://developer.blender.org/T94008) and
  [\#94292](http://developer.blender.org/T94292) automatically changes
  to the `OBJECT mode`
  (blender/blender-addons@8372ef96ade7,
  blender/blender-addons@2829c040f488).
  No error message should appear when the `EDIT mode` is initially
  active just before the import.

<!-- -->

- Fix in
  blender/blender-addons@b825c2d31ad3
  leads now to smooth sticks in an instancing vertice structure if
  option `Smooth` is set in the PDB importer.

<!-- -->

- Fix in
  blender/blender-addons@98003acc981d -
  sticks in instancing vertice structure: when decreasing the diameter
  of the sticks, e.g., with help of the `Utility Panel` (utility
  `Change stick size`), the equidistant planes of the mesh structure
  are not visible anymore. They now have a 100x smaller size. Note that
  these planes are needed for the instancing vertice structure.

<!-- -->

- Improvement of the color handling for atoms
  (blender/blender-addons@1b95d391dccb,
  blender/blender-addons@f1d2eca09bac,
  blender/blender-addons@c00916b26b08,
  blender/blender-addons@efcceb62d4a8,
  blender/blender-addons@7d11bb351e58,
  blender/blender-addons@ee7f95212b96,
  blender/blender-addons@6e910cf217f0,
  blender/blender-addons@7f5d0ab6beb3).
  As a standard shader, the [Principled BSDF
  shader](https://docs.blender.org/manual/en/dev/render/shader_nodes/shader/principled.html)
  is used for Cycles and Eevee. Also a few properties for Eevee are
  automatically set.

**Complete revision of the `Utility Panel`**

- Fix in
  blender/blender-addons@e54b9d2a4d81
  does not lead anymore to a crash of Blender when separating atoms from
  an instancing vertice structure.

<!-- -->

- Fix in
  blender/blender-addons@4009ff189d59
  does not lead anymore to an error message when using option `Default
  values` in the `Utility Panel`.

<!-- -->

- Fix in
  blender/blender-addons@84f5f4699232
  and
  blender/blender-addons@fd5697ebcf87:
  changing the material properties of atoms (utility `Change atom
  shape`) does also change the material properties of the related
  sticks if present.

<!-- -->

- All the object 'operators' in section `Change atom shape` got
  updated for color handling with respect to Eevee, Cycles and the
  Principled BSDF shader
  (blender/blender-addons@3012911034aa,
  blender/blender-addons@9be1af0f30f6).

<!-- -->

- Extension of the option `Custom data file`
  (blender/blender-addons@eb9a4e79d3df):
  all properties of the Principled BSDF shader can now be set in the
  ASCII data file for each element. Some important properties for Eevee
  can be set as well. With this, the sizes and material properties of
  selected atoms in an atomic structure can be changed at once. The user
  can therefore store her/his own material properties inside this file
  and use it at any time for any atomic structures. For more
  information, see the
  [documentation](https://docs.blender.org/manual/en/dev/addons/import_export/mesh_atomic.html).

## Compatibility Issues

- OBJ/MTL roughness conversion with Blender's own BSDF roughness
  parameter has changed, see
  blender/blender-addons@f26299bacc1.
