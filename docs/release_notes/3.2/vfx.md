# VFX & Video

## Sequencer

- Add filter method to strip transform
  (blender/blender@1c5f2e49b7bf)
- Use float for transformation offset
  (blender/blender@b4700a13c6ab)
- Now it is possible to create a new scene base on the active strip. The
  new scene is assigned to the active strip.
  (blender/blender@2d24ba0210e3)
- Add channel headers - provides ability to assign label and per channel
  mute / lock buttons.
  (blender/blender@277fa2f441f4)
- Enable edge panning for transform operator - when dragging strip out
  of view, it pans the view.
  (blender/blender@e49fef45cef7)
- Better handling of animation when transforming strips, transforming no
  longer extends selection.
  (blender/blender@32da64c17e9d)
- Add frame selected operator for preview.
  (blender/blender@e16ff4132e35)
- Thumbnail for first image now represents handle position
  (blender/blender@29b9187b327c)
- Add option to limit timeline view height
  (blender/blender@17769489d920)

## Compositor

- Add Combine and Separate XYZ Nodes
  (blender/blender@9cc4861e6f7)

## Clip Editor

- Fix tilt/scale slider of marker jumping on initial mouse grab
  (blender/blender@dd7250efb1a)
- Fix for tilt/scale slider to follow mouse exactly
  (blender/blender@00018bfaa2a)

## Motion Tracking

- Fixes very slow tracking with keyframe pattern matching when movie
  does not fit into cache
  (blender/blender@e08180fdab8)
