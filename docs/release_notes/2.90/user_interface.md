# Blender 2.90: User Interface

## Search

Operator search will now search through menu entries, to better show
relevant operators and where to find them in menus. Adding operators to
quick favorites or assigning shortcuts is now easy by right clicking on
search results.

For developers that need more low-level search, Developer Extras can be
enabled after which raw operators will appear in search results as well.
Add-ons that expose operators only through search
[need to be updated to expose them through a menu](python_api.md).

## Layout

- Checkbox placements was reworked to place text on the right hand side,
  group together related settings and make layouts more compact.
  (blender/blender@219049bb3b76,
  blender/blender@7fc60bff14a6)

![](../../images/Release_notes_properties_checkbox_split_layout.png){style="width:500px;"}
![](../../images/Release_notes_properties_checkbox_subpanel.png){style="width:500px;"}

- Shader nodes in the material properties editor now show socket colors
  matching the shader editor, and are positioned on the left side.
  (blender/blender@7ef2dd84246c,
  blender/blender@675d42dfc33d)  

![](../../images/Release_notes_node_socket_icons.png){style="width:500px;"}

## Statistics

- Scene statistics available in 3D viewport with an (optional)
  "Statistics" Viewport Overlay.
  (blender/blender@fd10ac9acaa0).

![Statistics Overlay](../../images/Statistics_Overlay.png){style="width:600px;"}

- Status Bar now showing only version by default, but context menu can
  enable statistics, memory usage, etc.
  (blender/blender@c08d84748804).

![Status Bar Context Menu](../../images/Status_Bar_Context_Menu.png){style="width:600px;"}

## Modifiers

Modifiers and other stacks now support drag and drop for reordering.

Their layouts were completely rewritten to make use of subpanels and the
current user interface guidelines.

Where they apply, the operation buttons ("Apply", "Copy" (now
"Duplicate"), and "Apply as Shape Key") are now in a menu on the right
side of the panel headers, along with new "Move to First" and "Move to
Last" buttons.

|Before|After|
|-|-|
|![](../../images/New-modifier-stack.png){style="width:300px;"}|![](../../images/Constraint_UI_2.90.png){style="width:300px;"}|

There are now shortcuts for common modifier operations.
(blender/blender@1fa40c9f8a81)
Enabled by default are:

- Remove: X, Delete
- Apply: Ctrl A
- Duplicate: Shift D

## About

- About Blender dialog added to the App menu to contain information like
  build, branch, and hash values. This declutters the Splash Screen and
  highlights only the release version.
  (blender/blender@5b86fe6f3350).

![](../../images/About18.png){style="width:600px;"}

## Miscellaneous

- Area borders given much larger hit area to make them easier to resize.
  (blender/blender@4e8693ffcddb)

![Increased Border Hit Area](../../images/Border_Hit_Size.png){style="width:600px;"}

- Support for Windows Shell Links (shortcuts) in File Browser. Extended
  Mac Alias usage. Better display of linked items.
  (blender/blender@1f223b9a1fce)

![Shortcuts and Aliases](../../images/Shortcuts.png){style="width:600px;"}

- Pie menus support for A-Z accelerator keys, for quickly selecting
  items with the keyboard.
- New "Instance Empty Size" setting in user preferences
  (blender/blender@e0b5a202311).
- Text field buttons now support local undo and redo while being edited.
  (blender/blender@1e12468b84d2).
- Outliner Delete operator added to delete all selected objects and
  collections using either the keymap or context menu.
  (blender/blender@ae98a033c856).
  Replaces the Delete Object and Delete Collection operators.
- Outliner Delete Hierarchy now operates on all selected objects and
  collections. Additionally, the menu option is shown for objects in the
  View Layer display mode.
  (blender/blender@26c0ca3aa7f4).
- Dragging outliner elements to an edge will scroll the view.
  (blender/blender@7dbfc864e6f8)
- Dragging panels to the edge of a region will scroll it up or down.
  (blender/blender@859505a3dae8)
- Move a few FFmpeg render properties from the scene properties to the
  audio output settings.
  (blender/blender@a55eac5107ed)
- Update the terminology used in some properties.
  (blender/blender@1278657cc2d),
  (blender/blender@3ea302cf8ef),
  (blender/blender@ef0ec014611)
- 3D Viewport: Add Edge Loopcut Slide to edge menu.
  (blender/blender@0fdb79fe584)
- The interface for many operators was updated to be clearer and more
  consistent with the rest of Blender.
  (blender/blender@17ebbdf1c17d)
- Option to print text in bold or italics style, synthesized from a
  single base UI font.
  (blender/blender@b74cc23dc478)
