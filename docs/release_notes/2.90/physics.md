# Blender 2.90: Physics

## Fluid

- New Features:
  - OpenVDB fluid caching: Smoke (grids) and liquid (grids and
    particles) data will now be cached into a single .vdb cache file per
    frame.
    (blender/blender@9fe64948abe9,
    blender/blender@995185894289)
- Usability:
  - Updated gravity: Matches world gravity now (fluid buoyancy behaves
    differently compared to 2.8x releases).
    (blender/blender@21485e94aac1)
  - Simplified cache format options:
    - There are now only two cache formats fields (Volume data - grids &
      particles, Surface data - meshes). Cache format options for
      'Noise' and 'Particles' have been deprecated.
    - Only two cache file formats are available ('.uni', '.vdb'). The
      '.raw' file format has been deprecated.
- New UI Options:
  - Frame Offset: Read cache files with a frame offset.
    ([Manual](https://docs.blender.org/manual/en/dev/physics/fluid/type/domain/cache.html),
    blender/blender@fb0f0f4d79a7)
  - System Maximum: Define the maximum number of particles that are
    allowed in a simulation.
    ([Manual](https://docs.blender.org/manual/en/dev/physics/fluid/type/domain/settings.html),
    blender/blender@e76f64a5329d)
- Important Bug Fixes:
  - Fixed issue with gas shading (shading in 'Replay' mode differed from
    shading in other cache modes).
    (blender/blender@f2b04302cdec,
    blender/blender@51f4bee5a5d7,
    blender/blender@7e64f6cee422,
    blender/blender@106e7654e857)

## Cloth

- New option to apply a pressure gradient emulating the weight of
  contained or surrounding fluid.
  (blender/blender@b1f97995084)
- Force effectors have a new Wind Factor setting to specify how much the
  force is reduced when acting parallel to the cloth surface (previously
  hard-coded as 100% reduction).
  (blender/blender@9e7012995)
