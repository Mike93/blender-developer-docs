# Blender 4.0: Add-ons

## Hydra Storm

Storm is a realtime renderer part of [USD](https://www.openusd.org).
When enabling the add-on, it can be chosen as an alternative to Cycles,
EEVEE or Workbench.
(blender/blender-addons@5ec7ca8f126586a065d9998de5d57cd7064814b1)

Compared to EEVEE its capabilities are limited, as it was designed
mostly for previews. It serves two main purposes:

- In production pipelines where Blender outputs USD files, it provides a
  preview of how the scene will be exported to other applications. Tools
  such as `usdview` use Storm as their default renderer, and other 3D
  apps often provide an option to use Storm as well.
- As a reference for Blender, add-on and studio pipeline developers to
  validate USD and Hydra integration.

When enabling "Developer Extras", there is an option to export the scene
through USD instead of Hydra. Hydra provides fast interactive updates,
but it currently does not match USD export exactly. USD gives an
accurate preview of how the file would be loaded into another
application. Work to improve this consistency is ongoing.

## Rigify

- The old interface for configuring layer names and defining the layer
  visibility panel toggle button layout, as well as the interface for
  specifying additional layers in rig component properties have been
  rewritten to use the new bone collections. Old metarigs have to be
  upgraded by clicking a button.
  (blender/blender-addons@5ec3aa7d9d5643a4f179c417285d7edba38b632c,
  [Manual
  1](https://docs.blender.org/manual/en/4.0/addons/rigging/rigify/metarigs.html#using-rig-types-advanced),
  [Manual
  2](https://docs.blender.org/manual/en/4.0/addons/rigging/rigify/metarigs.html#bone-collections-ui))
- The layer visibility button layout can now be modified through a
  visual editor sub-panel rather than through changing obscure number
  values.
  (blender/blender-addons@5ec3aa7d9d5643a4f179c417285d7edba38b632c,
  [Manual](https://docs.blender.org/manual/en/4.0/addons/rigging/rigify/metarigs.html#ui-layout-sub-panel))
- Re-generating the rig now preserves the visibility settings for bone
  collections that already existed in the target rig.
  (blender/blender-addons@729dc2b28433c8d1f20a61f0084d2b6c93604bef)
- The feature set list now contains ghost entries for the feature sets
  mentioned in the manual, containing brief descriptions and web links,
  even if they are not installed.
  (blender/blender-addons@08af2f5035f545c42b3be9ed11a567015bf20731)
- The new [Curved B-Bone vertex
  mapping](animation_rigging.md#bendy-bones)
  is now applied by the face upgrade operator for the mouth corners.
  (blender/blender-addons@ce6775b50d4e7231dc8c72715cdf49f3d3b83707)

The following rig features have been promoted from the [experimental
feature set](https://github.com/angavrilov/angavrilov-rigs) to the core
add-on:

- The leg rig now has an alternative IK to FK snap operator button that
  can use the heel roll control to avoid rotating the main IK control
  around certain local axes from its current orientation (by default it
  tries to keep the IK control parallel to the same imaginary ground
  plane).
  (blender/blender-addons@8f16645901838fa4e9c1c6df1c89cd08b2b3d430,
  [Manual](https://docs.blender.org/manual/en/4.0/addons/rigging/rigify/rig_features.html#legs))
- The leg rig can optionally generate a slider to perform forward roll
  around the tip of the toe rather than its base.
  (blender/blender-addons@8f16645901838fa4e9c1c6df1c89cd08b2b3d430,
  [Manual](https://docs.blender.org/manual/en/4.0/addons/rigging/rigify/rig_features.html#legs))
- There is a new built-in spline tentacle rig based on Spline IK.
  (blender/blender-addons@621149bb574cf5f1d35a8a6975d5bc3efd9e61ec,
  [Manual](https://docs.blender.org/manual/en/4.0/addons/rigging/rigify/rig_features.html#spline-tentacle))

## glTF 2.0

### Importer

- Implement EXT_mesh_gpu_instancing import
  (blender/blender-addons@01b30876b13edc51947af7e5bf84f76ddd7c014a)
- implement EXT_texture_webp import
  (blender/blender-addons@332839d7ab0af63473ccc951ef85a2458c13ea42)
- Better rigging roundrip when import then export (e7a7626052,
  c95149d351, 9b77553c6e)
- Fix, and add some conversion for custom attribute types
  (blender/blender-addons@9d5c1cc74a923edb837e8be99f199d80fb4cdb15,
  blender/blender-addons@9ccd934d19f7764f4958c8651d3225392cd36e23)
- Manage new Principled BSDF node
  (blender/blender-addons@7bc4614eff18a3f0db373f972d5d7998ae2aca44,
  blender/blender-addons@53f0f57ddf49630e57b2396594383407acde99a9
- Fix missing Volume extension in list of managed extensions
  (blender/blender-addons@62cf7b53c173f8ebfea72739bfe4cb716c9bff90)

### Exporter

- Manage new Principled BSDF node (Really long list of commit, see
  history for details)
- Use sparse accessors when it leads to small files
  (blender/blender-addons@ceac43149269a00a1916c370a46cdd16a5e27425)
- Add option to choose number of bone influence
  (blender/blender-addons@a75b59baa37902bcd36d36fdf004c9233d862305)
- Better rigging roundrip when import then export
  (blender/blender-addons@e7a762605251f5ddf66d1af6813e7ed256b3e987,
  blender/blender-addons@c95149d351851c2c80f721ed63e24fb8db1a9d3b,
  blender/blender-addons@9b77553c6e717824967e7382bd1dde7732e6fd10)
- Implement EXT_mesh_gpu_instancing export
  (blender/blender-addons@01b30876b13edc51947af7e5bf84f76ddd7c014a)
- Manage UVMaps outside of materials and using Attribute as UVMap
  (blender/blender-addons@bd7fea9ca3c5591a465a8ffefb064f0acd872072)
- implement EXT_texture_webp export
  (blender/blender-addons@332839d7ab0af63473ccc951ef85a2458c13ea42)
- Remove embedded gltf option
  (blender/blender-addons@097bf234f6ae90304cf37e768de2be0fc11e477f)
- Allow normalization in gather_attribute_change user extension
  (blender/blender-addons@1925b338278d7680fc7fee4b8fa6760f3c0123fc)
- Round tangents to avoid not determinist exports
  (blender/blender-addons@7f9569e6729134d7b63ae681438a8d77e6499b29)
- Texture: Use default value when merging channels
  (blender/blender-addons@dbcc17eb972b154ecc28e5612a3475f5531a55fc)
- Single Armature with no animation data will not export all anims
  (blender/blender-addons@15780ade327f81f7ba7d9b24f0b67df2416c58fc)
- Fix after API changes for nodegroups
  (blender/blender-addons@0cddda9150c02066caece6d67f41a2c9bdc0f194,
  blender/blender-addons@4df6f7c66d04afacaa213224ef63e6da54d9c2db)
- Fix crash when user try to merge identical tracks
  (blender/blender-addons@5180d77b7a10d0cec11d30856a8981af32b4081c)
- Fix crash on not valid meshes
  (blender/blender-addons@e3268f67c7912529ffeb67c64f0da8177e5d98d9)
- Fix wrong detection of Basis Shape Key
  (blender/blender-addons@0cc87647957b019e2cef2fcafda0a51a5a8fde9b)
- Fix check in texture image
  (blender/blender-addons@3f13ac814ff793dbd0f24ffd7a691eb865783656)
- Fix crash on extra channels when not sampled
  (blender/blender-addons@a4dd07ee34aebea02632282910b28cc855b96b0e)
- Fix crash in animation export in track mode
  (blender/blender-addons@a6cc6a16cb4eb80561cfed22000b23c562f9b8e2)
- Fix restore of track mute during track mode export
  (blender/blender-addons@462064c396491fc745ac15d865dfb5c56ffb0a11)
- Fix Real children of instance collection visibility check
  (blender/blender-addons@5a505e38eef47c480626f7d638aaecd5653ec6f0)

## Export UV Layout

UV Tiles can now be exported using the UDIM or UVTILE numbering schemes.
(blender/blender-addons@122534edfb8a0a9a4f84912f19e698fee50745dd)

## Collection Manager

Added support for the new alpha property in theme outlines to the QCD
Move Widget theme overrides.
(blender/blender-addons@acb39e7ad5709c0e18e0d0ec68ecbcdf77717357)
