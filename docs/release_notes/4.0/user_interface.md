# Blender 4.0: User Interface

## Search

- All regular dropdown and context menus can be searched by pressing
  spacebar.
  (blender/blender@35d3d52508).
- Add menus can be searched by immediate typing.
  (blender/blender@b688414223).
- Recently searched items are now at the top of search lists, with an
  option in the Preferences for disabling it.
  (blender/blender@8362563949).

## Text

- Default user interface font changed to Inter.
  (blender/blender@f58f6d0338).

![Inter font](../../images/Inter.png){style="width:400px"}

- Text output now more pleasing and typographically correct.
  (blender/blender@a0b4ead737)
- New user preference added for "subpixel anti-aliasing" for text
  output.
  (blender/blender@82bfc41d0c)
- Chinese, Japanese, and Korean punctuation characters now recognized in
  word selection.
  (blender/blender@6d64c6dcd7)
- Use multiplication symbol instead of letter "x" where appropriate.
  (blender/blender@9b4749e7c7)

## Elements

- Consistent top-down content ordering in menus. Menus no longer reverse
  order if opening upward.
  (blender/blender@b122faf705)
- All rotational inputs now shown with maximum precision.
  (blender/blender@b34ece48f8)
- Tree-view UIs draw hierarchy lines to visually communicate the nesting
  levels better.
  (blender/blender@71273df2d5)

![Hierarchy lines](../../images/Release_notes_4.0_tree_view_hierarchy_lines.png)

- Many lists now highlight the currently-selected item.
  (blender/blender@9f4b28bba8).
- List items that are not multiple select are now shown as radio
  buttons.
  (blender/blender@6dd3c90185).
- Resizing the toolbar no longer breaks snapping when dragged beyond the
  maximum available size.
  (blender/blender@248b322896)
- Windows and Linux: Color Picker can now pick outside of Blender
  windows.
  (blender/blender@5741a5d433,
  blender/blender@e5a0d11c4e)
- The Color Picker size has been increased.
  (blender/blender@0c7496f74d)

![Color picker size](../../images/PickerSize.png){style="width:600px;"}

- Improvements to Color Ramp drawing.
  (blender/blender@8a3766e241)
- Fix sidebar scrollbar overlapping category tabs when zooming.
  (blender/blender@080a00bda2)
- Fixed scrollbars being highlighted while outside of their region.
  (blender/blender@4f6785774a)
- Progress indicators now exposed to Python, including new ring version.
  (blender/blender@c6adafd8ef).

![](../../images/UpdatingProgress.png){style="width:200px""}

- Allow transparency when editing text in widgets.
  (blender/blender@2ec2e52a90)
- UI element outlines have changeable opacity, allowing for flat or
  minimalistic themes.
  (blender/blender@c033e434c5)

## Window

- Window title improved order and now includes Blender version.
  (blender/blender@636f3697ee).
- New Window now works while having an area maximized.
  (blender/blender@bb31df1054).
- Save Incremental in File menu, to save the current .blend file with a
  numerically-incremented name.
  (blender/blender@a58e5ccdec)
- Small changes to the File and Edit menus.
  (blender/blender@347e4692de)

## Files and Asset Browsers

- File Browser side bar now showing Bookmarks, System, Volumes, Recent.
  (blender/blender@9659b2deda)
- File and Asset Browser show a wait icon while previews are loading
  (blender/blender@4a3b6bfeac)
- Thumbnail views now allow any preview size from 16-256.
  (blender/blender@fa32379def)
- Image previews in File Browser now have checkerboard background if
  transparent.
  (blender/blender@e9e12015ea)
- File Browser now shows thumbnail previews for SVG images.
  (blender/blender@565436bf5f)

|Transparent Thumbnails|SVG Thumbnails|
|-|-|
|![](../../images/Transparent_Thumbnail.png)|![](../../images/SVGThumbnails.png)|

## Splash

Splash screen changes to make it easier for users to bring previously
saved settings into a new install.
(blender/blender@13f5879e3c)

![](../../images/SplashScreen.png){style="width:600px;"}

## Text Editor

- Support auto-closing brackets around the selection
  (blender/blender@96339fc3131789e6caa41f7ef01cc1edf5a38a28)
- Removed syntax highlighting support for LUA
  (blender/blender@95ca04dc207f8bbbdb18287223ba8fdabdd54511)

## Python Console

- Support cursor motion, selection & more text editing operations
  (blender/blender@18e07098ef25e7659e0c14e169a96b49c5a54cbf)
- Support tab-stops instead of expanding to spaces
  (blender/blender@a5cd4975433db6ab28f13c49d14a071f3ca8d2c2)

## Other Editors

- Preferences: layout and style tweaks
  (blender/blender@d0aa521ea8)
- Timeline: Invalid caches now show with striped lines.
  (blender/blender@8d15783a7e).
- Outliner: Drag & Drop now works between multiple windows
  (blender/blender@d102536b1a).
- Outliner: Select Hierarchy now works with multiple selected objects
  (blender/blender@594dceda7f).
- Top Bar and Status Bar colors now exactly match their theme colors
  (blender/blender@d86d2a41e0).

## Windows Integration

- Blend File association can now be done for all users or just the
  current users in the preferences. Unassociate is now available as
  well. Support for side-by-side installations was improved.
  (blender/blender@9cf77efaa0)
- Recent file lists are now per Blender version.
- Pinning a running Blender to the taskbar will properly pin the
  launcher.
- Multiple Blender version are available for Open With in Windows
  Explorer.
- Explorer "Quick Access" items added to File Browser System List
  (blender/blender@f1e7fe5492).

![](../../images/NewRegister1.png){style="width:600px;"}
![](../../images/NewRegister2.png){style="width:600px;"}

## Translations

The Catalan language is now one of six languages with **complete**
coverage.

![](../../images/Catalan.png){style="width:400px;"}
