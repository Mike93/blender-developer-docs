# Blender 4.1: Cycles

## OpenImageDenoise GPU Acceleration

OpenImageDenoise is now GPU accelerated on Intel and AMD GPUs. This makes full quality denoising available at interactive rates in the 3D viewport. (blender/blender#115045)

It is enabled automatically when using GPU rendering in the 3D viewport and for final renders.

NVIDIA and Apple GPU support is under development.

## Other

- Option to disable bump map correction
  (blender/blender@36e603c430ca90a4a19574a5071db54a7cebcb39)
- AMD GPU rendering support added for RDNA3 generation APUs
  (blender/blender@d19ad12b45c14dee9d17272cbc1d1b22d3a725aa)
- Linux CPU rendering performance was improved by about 5% across benchmarks 
  (blender/blender!116663)