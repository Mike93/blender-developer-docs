# Blender 4.1: Pipeline, Assets & I/O

## Stanford PLY

- PLY importer and exporter now supports custom vertex attributes
  (blender/blender@0eb6aef3b00).

## STL

- New STL (.stl) exporter
  (blender/blender@17c793e43c66ba).
  The new exporter is written in C++ and is 3x-10x faster than the
  Python exporter.
- Python STL exporter/importer add-on has been marked as legacy. Users of
`bpy.ops.export_mesh.stl` / `bpy.ops.import_mesh.stl` are advised to switch to
new STL exporter/importer functions `bpy.ops.wm.stl_export` /
`bpy.ops.wm.stl_import`.

## Alembic

- Velocities on Points are now read (blender/blender@472cf44166).
- Render resolution is now written to cameras (blender/blender@5412bd48a9).

## USD

- Export to a single root prim by default (blender/blender@b262655d39).
- Author extents property for lights on export (blender/blender@e6ff1348cb).
- Optionally author subdivision schema on export (blender/blender@5edda6cbcc).
- Scene graph instancing import support (blender/blender@ea89e11e01).
- Support armature and shape key export (blender/blender@c6d61e8586).

## OBJ

- Exporting completly flat or smooth shaded meshes can be up 20-40% faster (blender/blender@94ca3aea83).