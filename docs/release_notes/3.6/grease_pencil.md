# Grease Pencil

## Weight Paint

New Blur, Average, and Smear tools have been added to Weight Paint mode.
(blender/blender@397a14deff2d72b0e5614069c6de29b1e9efb084)

See [the
documentation](https://docs.blender.org/manual/en/3.6/grease_pencil/modes/weight_paint/tools.html).
