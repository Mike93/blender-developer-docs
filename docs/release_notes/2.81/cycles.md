# Blender 2.81: Cycles

## Denoising

A new Denoise node was added in the compositor, to denoise renders using
[OpenImageDenoise](https://openimagedenoise.github.io/). It needs Albedo
and Normal passes, which Cycles can render by enabling Denoising Data
passes.

Compared to the existing denoiser, it works better with more complex
materials like glass, and suffers less from splotchy artifacts. It also
gives better results with very low numbers of samples, which can be used
for quick previews.

|Before Denoising (4 samples)|After Denoising|
|-|-|
|![](../../images/Blender2.81_pabellon_4_oidn_before.png)|![](../../images/Blender2.81_pabellon_4_oidn_after.png)|

|Before Denoising (128 samples)|After Denoising|
|-|-|
|![](../../images/Blender2.81_pabellon_128_oidn_before.png)|![](../../images/Blender2.81_pabellon_128_oidn_after.png)|

This feature requires a CPU with SSE4.1, available in Intel and AMD CPUs
from the last 10 years.

## Shaders

Shader nodes for Cycles and Eevee have been extended.

- New White Noise Texture node, producing a random number based on input
  values.
  (blender/blender@133dfdd7)
- Noise Texture node: support 1D, 2D, and 4D noise.
  (blender/blender@23564583)
- Musgrave Texture node: support 1D, 2D, and 4D musgrave.
  (blender/blender@f2176b3f)
- Voronoi Texture node: support 1D, 2D, and 4D voronoi. Add more feature
  types.
  (blender/blender@613b37bc)
- New Volume Info node, for convenient access to Color, Density, Flame,
  and Temperature attributes of smoke domains.
  (blender/blender@e83f0922)
- Object Info node: new Object Color output.
  (blender/blender@08ab3cbc)
- New Map Range node. Linearly remap a value from one range to another.
  (blender/blender@71641ab5)
  (blender/blender@7a7eadaf)
- New Clamp node. Clamps a value between a maximum and a minimum values.
  (blender/blender@313b7892)
- Math node: shows one or two inputs depending on how many are needed by
  the operation.
  (blender/blender@e5618725)
- Vector Math node: new operations, and show number of inputs depending
  on the operation.
  (blender/blender@7f4a2fc4)
- Mapping node: location, rotation and scale are now node inputs that
  can be linked to other nodes.
  (blender/blender@baaa89a0bc5)
- New Vertex Color node, for convenient access to Vertex Colors and
  their alpha channel.
  (blender/blender@2ea82e86)

![New voronoi texture nodes](../../images/Blender2.81_voronoi.png){style="width:600px;"}

## NVIDIA RTX

Cycles now has experimental support for rendering with
hardware-accelerated raytracing on NVIDIA RTX graphics cards. To use,
enable OptiX in Preferences \> System \> Cycles Render Devices.

Most, but not all features of the CUDA backend are supported yet:
Currently still missing are baking, branched path tracing, ambient
occlusion and bevel shader nodes and combined CPU + GPU rendering. So
certain scenes may need small tweaks for optimal rendering performance
with OptiX. When rendering the first time, the OptiX kernel needs to be
compiled. This can take up to a few minutes. Subsequent runs will be
faster.

OptiX requires [recent NVIDIA drivers](https://www.nvidia.com/Download/index.aspx) and is supported on
both Windows and Linux.

- Linux: driver version 435.12 or newer
- Windows: driver version 435.80 or newer

![OptiX performance compared to CPU and CUDA](../../images/Blender2.81_cycles-rtx-performance-1.png){style="width:600px;"}

The OptiX backend was [contributed by NVIDIA](https://code.blender.org/2019/07/accelerating-cycles-using-nvidia-rtx/).

## Adaptive Subdivision

Cycles adaptive subdivision now stitches faces to avoid cracks between
edges, that may be caused by displacement or different materials.

![](../../images/Cycles_displacement_cracks.jpg)
![Adaptive subdivision with and without cracks](../../images/Cycles_multi_material_cracks.jpg)

## Other

- Reduced shadow terminator artifacts for bump mapping with diffuse
  BSDFs.

|Before|After|
|-|-|
|![](../../images/Blender2.81_bump_before.png)|![](../../images/Blender2.81_bump_after.png)|

- Viewport support to use HDRI lighting instead of scene lighting, for
  look development.
- Viewport option to display a render pass, instead of the combined
  pass.
