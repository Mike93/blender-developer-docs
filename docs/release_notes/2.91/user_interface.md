# Blender 2.91: User Interface

## Outliner

- Collection colors: object collections now have a color tag, set from
  the right-click menu.
  (blender/blender@33d7b36cecd4)
  (blender/blender@16f625ee654)
  (blender/blender@7b3d38a72d3)

![](../../images/Outliner-colors.png)

- Move mode toggling to the left column. When in non-object modes, icon
  buttons are shown in the left column for toggling modes. Clicking a
  dot icon will switch that object to become the new active object,
  ctrl-click adds the object into the current mode.
  (blender/blender@2110af20f5e6)

<!-- -->

- Add left and right walk navigation. When the active element is
  expanded, walking right will select the first child element. When the
  active is closed, walking left will select the parent element.
  (blender/blender@70151e41dc02)
- The collection exclude checkbox was moved to the right with the other
  restriction toggles.
  (blender/blender@8bce181b71545)
- Use shift-click on restrict icons for setting visibility and
  selectability of bone children to be consistent with objects and
  collections.
  (blender/blender@9de18c361bb4)
- Drag & drop for modifiers, constraints and grease pencil effects.
  (blender/blender@1572da858df)
  - Drag within the object or bone to reorder
  - Drag to another object or bone to copy
  - Drag the parent element (Modifiers, Constraints, or Effects) to link
    all to the target object or bone.
- Use the right-click target element to determine the type of context
  menu. For example, a right-click on a modifier will show the modifier
  menu even if objects are selected.
  (blender/blender@b0741e1dcbc5)
- Display grease pencil modifiers and shader effects in the tree
  (blender/blender@2c34e09b08fb)
- Object data (can now be dropped into the 3D Viewport to create object
  instances), especially useful for orphan meshes, curves.. etc.
  (blender/blender@e56ff76db5b44ae6784a26ff6daf9864b2387712)
- Rename "Sequence" Display Mode to "Video Sequencer"
  (blender/blender@af843be3012)

## Property Search

Search is now possible in the property editor, with automatically
expanding and collapsing panels. `Ctrl-F` starts a search, and
`alt-F` clears the search field.
(blender/blender@bedbd8655ed1,
blender/blender@8bcdcab659fb,
blender/blender@7c633686e995)

Tabs without search results will appear grayed out in the tab bar on the
left. If a result is not found in the current tab, the search switches
to the next one with a search result.

<figure>
<video src="../../../videos/Property_Search_2.91_Video.mp4" title="Searching across all tabs in the property editor" controls=""></video>
<figcaption>Searching across all tabs in the property editor</figcaption>
</figure>

## Other

- Masking: Update the mask menu to be more consistent with other editors
  by rearranging and adding missing operators.
  (blender/blender@c598e939ad2),
  (blender/blender@4155d77026d).
- Menus: Aesthetic tweaks to Select All by Type operator
  (blender/blender@8f4f9275cea)
- UV Editor: Add missing Unwrapping tools to UV menu
  (blender/blender@4e3cb956385)
- 3D Viewport: Move Live Unwrap from UV menu to toolbar
  (blender/blender@637699e78ab)
- Many searches now support fuzzy and prefix matching.
  (blender/blender@98eb89be5dd08f3)
- Add temperature units option with support for Kelvin, Celsius and
  Fahrenheit.
  (blender/blender@36aeb0ec1e2e)
  - Note that math operations are not supported for non-Kelvin
    temperature units, and will lead to incorrect results.
- Move curve geometry "start and end mapping" to a new subpanel.
  (blender/blender@4e667ecef92f)
- Auto keyframing settings are moved to their own popover in the
  timeline header.
  (blender/blender@099ce95ef36d)

![](../../images/Auto_Keyframing_Popover_2.91.png)

- Option to invert filter in Dopesheet, Timeline and Curve Editor.
  (blender/blender@fecb276ef7b3)
- Change the invert list filter icon to be consistent with other areas.
  (blender/blender@459618d8606e)
- "Reset to Default Value" in button right click menus now works in many
  more places, including modifiers and particle settings
  ([Reference](https://developer.blender.org/T80164#1034116))
- More information is now shown in tooltips in the "Open Recent Files"
  menu: file path, size, and modification date
  (blender/blender@53792e32e7dc).

![](../../images/Recent_Details.png){style="width:600px;"}

- Changes to "Quit" and "About" dialogs with change to monochrome
  versions of the large alert icons (and blender logo).
  (blender/blender@dc71ad062408).

![](../../images/Monochrome_Alerts.png){style="width:400px;"}

- Float inputs should never show the confusing value of negative zero.
  (blender/blender@c0eb6faa47ec).
- Categorized List Menus get slight change in format with nicer
  headings.
  (blender/blender@aa244a7a68db).

![](../../images/Categorized_Menu.png){style="width:700px;"}
