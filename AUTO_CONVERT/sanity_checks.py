#!/usr/bin/python3

"""
Just some of checks that run on the markdown files to find common issues. We
could attempt to auto-fix some of them, but let's first try to detect them
reliably and see if it's worth automating even.
"""

import os
import pathlib

scripts_path = pathlib.Path(os.path.abspath(__file__)).parent
docs_path = scripts_path.parent / "docs"

log_file = open(scripts_path / "sanity_checks.log", "w")


def warn(text, filepath):
    full_text = f"WARNING: {filepath}\n  {text}"
    print(full_text)
    log_file.write(full_text + "\n")


def check_name(name):
    if name.lower() != name:
        warn("File/directory names should be lower case only",
             os.path.join(root, name))
    if "-" in name or " " in name:
        warn("File/dircectory names should not contain '-' characters or spaces",
             os.path.join(root, name))


def check_h1_headers(open_file, filepath):
    has_h1 = False
    inside_code_block = False
    for line in open_file:
        if line.startswith("```"):
            inside_code_block = not inside_code_block
        if inside_code_block:
            continue

        if not line.startswith("# "):
            continue

        if has_h1:
            warn("File contains multiple H1 level headers (single '#')", filepath)
            return
        has_h1 = True


for root, directories, files in os.walk(docs_path):
    md_files = [file for file in files if file.endswith(".md")]

    # 1. Check if file and directory names are all lower case and using hyphens.
    for directory in directories:
        check_name(directory)
    for file in md_files:
        check_name(file)

    # 2. Check if more than one H1 level header
    for file in md_files:
        filepath = os.path.join(root, file)
        with open(filepath) as fp:
            check_h1_headers(fp, filepath)
