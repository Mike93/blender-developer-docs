#!/usr/bin/python3

import os
import pathlib
import re
import requests
import subprocess
import tempfile
import time

DOWNLOAD = False
OVERRIDE_OUTPUT = False

scripts_path = pathlib.Path(os.path.abspath(__file__)).parent
out_path = scripts_path.parent / "docs"
wiki_path = scripts_path / "wiki"
pages_path = scripts_path / "pages.txt"
remap_path = scripts_path / "remap.txt"
redirects_path = scripts_path / "redirects.txt"
pageinfo_path = scripts_path / "pageinfo.txt"
images_path = out_path / "images"
videos_path = out_path / "videos"

tmp_dir = tempfile.TemporaryDirectory()
tmp_path = pathlib.Path(tmp_dir.name)

def log(text):
    print(text)


# Downloaded wiki pages with templates expanded
orig_pages = pages_path.read_text().strip().split('\n')
pages = [page.strip('/').replace(' ', '_') for page in orig_pages]
convert_paths = []
known_md_links = set()
known_directories = set()

remap_lines = remap_path.read_text().strip().split('\n')
remaps = []
for line in remap_lines:
    line = line.strip()
    if len(line):
        remap_from, remap_to = line.split(" ", 1)
        remap_from = remap_from.lower().replace("-", "_")
        remaps += [(remap_from, remap_to)]


def remap_page(page):
    page = page.lower().replace("-", "_")
    for remap_from, remap_to in remaps:
        if page.startswith(remap_from):
            relative_page = page.split(remap_from, 1)[1]
            # Make sure we only remap at word boundaries (considering '/' and ':' as delimiters).
            if relative_page and relative_page[0] not in {"/", ":"}:
                continue

            page = page.replace(remap_from, remap_to, 1)
    return page


for page in pages:
    if page.find("/") != -1:
        known_directories.add(page.rsplit('/', 1)[0])

redirects = []
for page in pages:
    new_page = remap_page(page)

    # Hardcoded/manual changes
    if page == "Reference/Release_Notes/4.0/Animation_Rigging/Bone_Collections_&_Colors_Upgrading":
        redirects += [f"Redirect permanent /wiki/{page} https://developer.blender.org/docs/release_notes/4.0/upgrading/bone_collections/"]
        continue
    if new_page.startswith("trash/"):
        continue

    if new_page.endswith("/index"):
        new_page = new_page.removesuffix("/index")
    redirects += [f"Redirect permanent /wiki/{page} https://developer.blender.org/docs/{new_page}/"]
# Order matters for the redirects: children before parents, because the first match will be used, the remaining path appended.
redirects.reverse()

# Redirect user pages to archived version.
redirects += ["RedirectMatch permanent ^/wiki/User(.*)$ https://archive.blender.org/wiki/2024/wiki/User$1.html"]
redirects += ["RedirectMatch permanent ^/index.php/User(.*)$ https://archive.blender.org/wiki/2015/index.php/User$1"]
redirects += ["Redirect permanent /wiki/Main_Page https://developer.blender.org/docs/"]
redirects += ["Redirect permanent /wiki/ https://developer.blender.org/docs/"]
redirects += ["Redirect permanent /Main_Page  https://developer.blender.org/docs/"]
redirects += ["Redirect permanent /  https://developer.blender.org/docs/"]

redirects_path.write_text("\n".join(redirects))

pageinfo = []
for i, page in enumerate(pages):
    # Windows doesn't support `:` in file names. Strip those.
    sanitized_page = page.replace(':', '')
    page_path = wiki_path / pathlib.Path(sanitized_page + ".wiki")
    md_path = out_path / pathlib.Path(remap_page(sanitized_page) + ".md")

    if str(page_path.with_suffix("").relative_to(wiki_path)) in known_directories:
        md_path = md_path.with_suffix("") / "index.md"

    convert_paths += [(page_path, md_path)]
    known_md_links.add(str(md_path))

    if not DOWNLOAD:
        continue

    if page_path.exists():
        # print(f"CACHED {page}")
        continue

    params = {
        "action": "expandtemplates",
        # Template expansion may depend the name of the page using it.
        "title": orig_pages[i],
        "text": "{{:" + page + "}}",
        "prop": "wikitext",
        "format": "json"}

    response = requests.get(
        url="https://wiki.blender.org/w/api.php", params=params)
    if response.status_code == requests.codes.ok:
        log(f"GOT {page_path.relative_to(wiki_path)}")
        text = response.json()['expandtemplates']['wikitext']
        page_path.parent.mkdir(exist_ok=True, parents=True)
        page_path.write_text(text)
    else:
        log(f"ERROR {response.status_code} {page_path.relative_to(wiki_path)}")

    # Get metainformation (creation date, modification date, number of edits).

    params = {
        "action": "query",
        "prop": "info|revisions",
        # rv-fields used to get info about the revisions (creation date and number of revisions/edits)
        "rvlimit": 300,
        "rvprop": "timestamp",
        "rvdir": "newer",
        "titles": page,
        "format": "json"}

    response = requests.get(
        url="https://wiki.blender.org/w/api.php", params=params)
    if response.status_code == requests.codes.ok:
        query = response.json()['query']

        basicpageinfo = list(query['pages'].values())[0]
        touched_datestr = basicpageinfo['touched']
        created_datestr = basicpageinfo['revisions'][0]['timestamp']
        edits = len(basicpageinfo['revisions'])

        pageinfo += [f"{page} | {md_path.relative_to(out_path)} | {created_datestr} | {touched_datestr} | {edits}"]
    else:
        log(f"ERROR {response.status_code} {page_path.relative_to(wiki_path)}")

    time.sleep(1.0)

if pageinfo:
    pageinfo_path.write_text("\n".join(pageinfo))

files = []

# Convert mediawiki to markdown
for page_path, md_path in convert_paths:
    if not page_path.exists():
        continue
    if not OVERRIDE_OUTPUT and md_path.exists():
        continue

    md_path.parent.mkdir(exist_ok=True, parents=True)

    #log(f"CONVERT {md_path.relative_to(out_path)}")

    # Gather files, and make paths relative
    def detect_file(matchobj):
        global files
        filename = matchobj.group(1).strip().replace(" ", "_")
        if filename.endswith(".mov") or filename.endswith(".mp4") or filename.endswith(".webm"):
            filepath = videos_path / filename
        else:
            filepath = images_path / filename
        files += [filepath]
        filepath = os.path.relpath(filepath, md_path.parent)
        return matchobj.group(0).replace(matchobj.group(1), filepath)

    wiki_text = page_path.read_text()
    wiki_text = re.sub(r"\[\[File:([\w. -_]+)\]\]", detect_file, wiki_text)
    wiki_text = re.sub(r"\[\[File:([\w. -_]+)\|", detect_file, wiki_text)
    wiki_text = re.sub(r"\[\[Image:([\w. -_]+)\]\]", detect_file, wiki_text)
    wiki_text = re.sub(r"\[\[Image:([\w. -_]+)\|", detect_file, wiki_text)
    # Workaround table parse error with pandoc.
    wiki_text = re.sub(r"^ \|", "|", wiki_text, flags=re.M)
    wiki_text = wiki_text.replace(" [[:Template:Css/prettytable]]", "")

    tmp_page_path = tmp_path / page_path.name
    tmp_page_path.write_text(wiki_text)
    try:
        result = subprocess.run(
            ["pandoc", "--from", "mediawiki", "--to", "gfm", tmp_page_path, "-o", md_path])
        if result.returncode != 0:
            raise Exception(f"pandoc returned return code {result.returncode}")
    except Exception as e:
        log(
            f"ERROR Could not convert file {page_path.relative_to(wiki_path)} -- SKIPPING")
        log("    " + str(e))
        continue

    # Fix links to be relative
    def relative_link(matchobj):
        link = matchobj.group(1)
        if link.startswith("http:") or link.startswith("https:"):
            return matchobj.group(0)

        page_link = link
        internal_link = None
        if link.find('#') != -1:
            page_link, internal_link = link.split('#', 1)

        md_link = ""
        if page_link:
            if page_link.startswith('/'):
                page_link = str(md_path.parent) + page_link

            page_link = remap_page(page_link)
            if page_link.startswith("http:") or page_link.startswith("https:"):
                return matchobj.group(0).replace(link, page_link)

            md_link = out_path / page_link / "index.md"
            if str(md_link) not in known_md_links:
                md_link = out_path / (page_link + ".md")
                if str(md_link) not in known_md_links:
                    log(f"MISSING LINK in {md_path.relative_to(out_path)}: {link}")
                    return matchobj.group(0)

            md_link = os.path.relpath(md_link, md_path.parent)

        if internal_link:
            md_link += "#" + internal_link.lower().replace('_', '-')
        return matchobj.group(0).replace(link, md_link)

    if md_path.exists():
        md_text = md_path.read_text()
        md_text = re.sub('\]\(([\w. -_]+) "wikilink"\)',
                         relative_link, md_text)
        md_text = md_text.replace(' "wikilink")', ")")
        md_text = md_text.replace('\\`', "`")  # Backtickcode wiki extension
        md_text = md_text.replace('\\_', "_")  # Backtickcode wiki extension
        # Wiki keywords
        md_text = md_text.replace('\n__NOEDITSECTION__\n', "")
        md_text = md_text.replace('__TOC__', "")
        md_path.write_text(md_text)

# Download files
for i, filepath in enumerate(files):
    if not DOWNLOAD:
        continue

    progress = f"{filepath.name} ({i+1} / {len(files)})"

    if filepath.exists():
        # print(f"CACHED {progress}")
        continue

    filepath.parent.mkdir(exist_ok=True, parents=True)

    url = "https://wiki.blender.org/wiki/Special:Redirect/file/" + filepath.name
    response = requests.get(url)
    if response.status_code == requests.codes.ok:
        log(f"GOT {progress}")
        with open(filepath, "wb") as f:
            f.write(response.content)
    else:
        log(f"ERROR {response.status_code} {progress}")

    time.sleep(1.0)

log("\nConversion done.")
