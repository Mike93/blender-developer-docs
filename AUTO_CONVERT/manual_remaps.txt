# Further remaps that are not handled by the auto conversion remapping. This way
# redirects can be set up for manual changes after the conversion.

# Removed "New Developers" index page, "New Developer Overview" is new landing page.
handbook/getting_started/new_developers/overview handbook/getting_started/new_developers/index

# Removed "Automated Testing" index page, "Setup" is new landing page.
handbook/testing/setup handbook/testing/index

# File was converted manually at different location

release_notes/4.0/animation_rigging/bone_collections_&_colors_upgrading release_notes/4.0/upgrading/bone_collections
Source/Nodes/Modifier_nodes features/nodes/
