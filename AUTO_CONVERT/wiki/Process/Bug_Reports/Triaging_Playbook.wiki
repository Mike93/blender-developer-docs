=Blender Bug Triaging Playbook=

<div class="bd-lead">

This document should cover possible scenarios that can be encountered when triaging bugs including example responses and actions to take.

</div>

This assumes you are already familiar with gitea (blenders bugtacking system) and the bug fixing process as a whole.
For additional information, reading [[Process/Help_Triaging_Bugs|How to help triaging bugs?]] and [[Process/A Bugs Life|A Bugs Life]] is encouraged.
If you encounter a situation that is not clear what to do, then is probably a new scenario (and should be added below).


{|class="note"
|-
|<div class="note_title">'''Canned Responses'''</div>
<div class="note_content">Unlike our previous bugtracker system, the message examples present here are not incorporated in gitea's UI (yet), these need to be copy/pasted from this page instead for now. The actions and example tasks are only present here on the wiki.</div>
|}



{|class="note"
|-
|<div class="note_title">'''Priority Clarifications'''</div>
<div class="note_content">
* ''Unbreak Now!'' - Leave for [[Modules|module owners]], but contact the responsible person on [https://blender.chat blender.chat] if you think you found one.
* ''High'' - Most crashes and recent regressions.
* ''Normal'' - Regular valid bug, old or new.
* ''Low'' - Small annoyance in obscure workflows.

''Note: When finding confirmed reports for new features, feel free to make sure the responsible developer is aware of those (by subscribing the developer to the issue). Keep in mind that in some cases the "fix" is about mentioning those limitations in the documentation.''

''Note: When finding confirmed recent (less than 6 months old) regressions, make sure the responsible developer is aware of those (by subscribing the developer to the issue)''
</div>
|}


'''Missing Scenarios:'''

Add them here if you want them discussed before we create an action template for them.
* Issue building blender (these should be handled [https://devtalk.blender.org/c/blender/building-blender/11 here] instead

'''Spam Scenario:'''
* Task is spam

'''Should Close Task Scenarios:'''
* No reply after a week
* Low quality report
* Too complex file report, where user could not simplify enough
* Feature request
* Accidental feature request
* Request for support
* Multi-report (many reports in a single report)
* Valid report, but fixed already
* Complete report, but on a feature branch
* Complete report, but out of memory
* Complete report, but performance
* Complete report, but corrupted mesh
* Complete report, but dependency cycle
* Unsupported graphics card or driver
* Blender doesn't run / crashes

'''Need More Information Scenarios:'''
* Unexpected UI/Tool behaviour
* Complete report, but cannot reproduce

'''Valid Bug Reports:'''
* Valid report general instructions
* Valid crash report, can confirm
* Valid crash report, can confirm it crashes "at random"
* Valid generic report, can confirm, known working version

==Spam Scenario==
===Task is spam===
'''Example:''' [http://developer.blender.org/T70290 #70290]

'''Action:''' 
* Close the report. 
* Change the `Status >` label to `Status > '''Archived'''` (a bot should do this automatically - but only visible after a manual refresh of the page). 
* Re-title report to "Removed spam", and empty the description and subscribers. 
* Notify Thomas Dinges (@ThomasDinges) in blender.chat with link to the bug report for further action regarding user account.

==Should Close Task Scenarios==

===No reply after a week===

'''Example:''' [http://developer.blender.org/T72496 #72496]

'''Action:''' 
* If the report status is "Needs Information From User" but there was no reply within a week, Close the report. 
* Change the `Status >` label to `Status > '''Archived'''` (a bot should do this automatically - but only visible after a manual refresh of the page).
* Explain our policy.


<div class="card bg-light">
<div class="card-body">
'''Message Example'''

<tt>
No activity for more than a week. As per the tracker policy we assume the issue is gone and can be closed.

Thanks again for the report. If the problem persists please open a new report with the required information.</tt>
</div>
</div>


===Low quality report===
'''Example:''' [http://developer.blender.org/T73084 #73084], [http://developer.blender.org/T56805 #56805], [http://developer.blender.org/T69448 #69448]

'''Action:''' 
* Close the report. 
* Change the `Status >` label to `Status > '''Archived'''` (a bot should do this automatically - but only visible after a manual refresh of the page).
* Explain that report doesn’t contain enough information.


<div class="card bg-light">
<div class="card-body">
'''Message Example'''

<tt>
This report does not contain all the requested information, which is required for us to investigate the issue.

Please submit a new report and carefully follow the instructions. Be sure to provide system information, Blender version, the last Blender version which worked, and a .blend file with exact steps to reproduce the problem.

A guideline for making a good bug report can be found at https://wiki.blender.org/wiki/Process/Bug_Reports
</tt>
</div>
</div>


===Too complex file report, where user could not simplify enough===
'''Example:''' [http://developer.blender.org/T69566 #69566]

'''Note:''' Python code is prone to introduce errors, so we are more strict on drawing the line for simplification in these cases.

'''Action:''' 
* Close the report. 
* Change the `Status >` label to `Status > '''Archived'''` (a bot should do this automatically - but only visible after a manual refresh of the page).
* Explain our limitations in tackle complex files.


<div class="card bg-light">
<div class="card-body">
'''Message Example'''

<tt>
Thanks for the report. Unfortunately the scenario described is too time consuming for us to track down, we require the bug reporter to narrow down the problem.

Normally .blend files can be simplified by removing most objects and disabling settings, until the problem reveals itself more clearly.

If there are many steps to reproduce the problem, please attach a .blend file saved right before the bug occurs, so only a few steps are needed to trigger the bug.
</tt>
</div>
</div>


===Feature request===
'''Example:''' [http://developer.blender.org/T69676 #69676]

'''Action:''' 
* Close the report. 
* Change the `Status >` label to `Status > '''Archived'''` (a bot should do this automatically - but only visible after a manual refresh of the page).
* Direct the user to the proper place for feature requests.


<div class="card bg-light">
<div class="card-body">
'''Message Example'''

<tt>
Thanks for the report, but please use other channels for user feedback and feature requests:
https://wiki.blender.org/wiki/Communication/Contact#User_Feedback_and_Requests

For more information on why this isn't considered a bug, visit: https://wiki.blender.org/wiki/Reference/Not_a_bug
</tt>
</div>
</div>


===Accidental feature request===
'''Example:''' [http://developer.blender.org/T66861 #66861]

'''Note 1:''' Users may use tools for purposes they weren’t originally intended to solve and find they behave poorly. Even though it may seem like a bug. If they are trying to use functionality in a way it’s not intended - then it’s not considered a bug.

'''Note 2:''' If this seems like a generally useful improvement, we can consider adding it as a TODO as long as a developer has time to work on this. See: T63725

'''Action:''' 
* Close the report. 
* Change the `Status >` label to `Status > '''Archived'''` (a bot should do this automatically - but only visible after a manual refresh of the page).
* Explain that we do only bugs here.


<div class="card bg-light">
<div class="card-body">
'''Message Example'''

<tt>
Thanks for the report, but the issue reported here is a request for modified/improved behavior and not a bug in current behavior. Closing as this bug tracker is only for bugs and errors.

For user requests and feedback, please use other channels:
https://wiki.blender.org/wiki/Communication/Contact#User_Feedback_and_Requests

For more information on why this isn't considered a bug, visit: https://wiki.blender.org/wiki/Reference/Not_a_bug
</tt>
</div>
</div>


===Request for support===

'''Action:''' 
* Close the report. 
* Change the `Status >` label to `Status > '''Archived'''` (a bot should do this automatically - but only visible after a manual refresh of the page).


<div class="card bg-light">
<div class="card-body">
'''Message Example'''

<tt>
For help using Blender, please try one of the community websites:
https://www.blender.org/community/

If you think you found a bug, please submit a new report and carefully follow the instructions. Be sure to provide system information, Blender version, and a .blend file with exact steps to reproduce the problem.
</tt>
</div>
</div>


===Multi-report===
'''Example:''' [http://developer.blender.org/T39616 #39616]

'''Action:''' 
* Close the report. 
* Change the `Status >` label to `Status > '''Archived'''` (a bot should do this automatically - but only visible after a manual refresh of the page).
* Reply that users may edit to only include a single issue, opening new reports for all other issues.


<div class="card bg-light">
<div class="card-body">
'''Message Example'''

<tt>
Thanks for the report. However to properly track issues we need them to be reported separately.

Please submit a new report for every individual bug.
</tt>
</div>
</div>


===Valid report, but fixed already===
'''Example:''' [http://developer.blender.org/T69806 #69806]

'''Note 1:''' To be sure of this you need to have tested with both the reported blender version, and the latest (main branch) revision or daily build.

'''Note 2:''' If you don't have the blender version used, and cannot reproduce the crash in any old version DO NOT close this report, refer to the "Complete report, but cannot reproduce".

'''Action:''' 
* Close the report. 
* Change the `Status >` label to `Status > '''Resolved'''`
* Be explicit on the versions used to test and ask users to download a new blender. 
* (Optional) find the commit that fixed the issue (e.g. by bisecting) and leave this as a comment (this can help to determine if this commit should be backported to LTS).



<div class="card bg-light">
<div class="card-body">
'''Message Example'''

<tt>
I reproduced this bug in the latest official release, but not the latest daily build, so it appears the bug has been fixed already.

Please try the latest daily build:
https://builder.blender.org/download/

If the problem persists, please let us know so we can re-open the report. Don't forget to mention the specific version you tested again.
</tt>
</div>
</div>


===Valid report, but duplicate exists===

'''Action:''' 
* Mention the duplicate issue in a comment (reference its issue number including the dash character, e.g. `#12345` so this correctly shows up in the report this will get merged to) 
* Close the report. Change the `Status >` label to `Status > '''Duplicate'''`
* Ask the user to subscribe in the report this is merged to (this will not happen automatically).



<div class="card bg-light">
<div class="card-body">
'''Message Example'''

<tt>
I can confirm, however this issue has been reported before, see #12345.
Will merge reports. Please subscribe in #12345 if you want to see further updates
</tt>
</div>
</div>


===Complete report, but on a feature branch===
'''Example:''' [http://developer.blender.org/T89566 #89566], [http://developer.blender.org/T88388 #88388], [http://developer.blender.org/T88591 #88591], [http://developer.blender.org/T91108 #91108]

'''Action:''' 
* Close the report. 
* Change the `Status >` label to `Status > '''Archived'''` (a bot should do this automatically - but only visible after a manual refresh of the page).
* Explain that feature branches are not open for bug reports.


<div class="card bg-light">
<div class="card-body">
'''Message Example'''

<tt>
Thanks for the report. It appears that this is reporting on a feature branch.

Feature branches might include (partially) broken or missing functionality (which most of the time developers are already aware of). For this reason, bugs in feature branches are not handled on the bug tracker.

Please also test this on the master branch and if the issue is present there as well, please submit a new report for this.
</tt>
</div>
</div>


===Complete report, but out of memory===
'''Example:''' [http://developer.blender.org/T69415 #69415]

'''Action:''' 
* Close the report. 
* Change the `Status >` label to `Status > '''Archived'''` (a bot should do this automatically - but only visible after a manual refresh of the page).
* Explain that the user needs more memory for production scenes.


<div class="card bg-light">
<div class="card-body">
'''Message Example'''

<tt>
Thanks for the report. It appears this crashes because Blender ran out of memory.

While we want to improve Blender to handle system running out of memory gracefully, this is not currently considered a bug.
</tt>
</div>
</div>


===Complete report, but performance===
'''Example:''' [http://developer.blender.org/T59438 #59438]

'''Action:''' 
Unless this is a lag with decent hardware, recent drivers and not-so-insane geometry/shader (or performance regressed from a previous version of blender):
* Close the report. 
* Change the `Status >` label to `Status > '''Archived'''` (a bot should do this automatically - but only visible after a manual refresh of the page).


<div class="card bg-light">
<div class="card-body">
'''Message Example'''

<tt>
Thanks for the report. While we do continue to work on improving performance in general, potential performance improvements are not handled as bug reports.

To improve performance, consider using less complex geometry, simpler shaders and smaller textures.
</tt>
</div>
</div>


===Complete report, but corrupted mesh===
'''Example:''' [http://developer.blender.org/T57178 #57178]

'''Action:''' 
Unless we have a way to reproduce it from scratch with the latest Blender:
* Close the report. 
* Change the `Status >` label to `Status > '''Archived'''` (a bot should do this automatically - but only visible after a manual refresh of the page).
* Mention that corruptions can be found (and corrected) using [https://docs.blender.org/api/latest/bpy.types.Mesh.html#bpy.types.Mesh.validate Mesh.validate] and/or [https://docs.blender.org/api/latest/bpy.types.Mesh.html#bpy.types.Mesh.validate_material_indices Mesh.validate_material_indices]


<div class="card bg-light">
<div class="card-body">
'''Message Example'''

<tt>
Thanks for the report. Unfortunately unless there are clear instructions on how to reproduce it from scratch we cannot debug this any further.

Things that may cause mesh corruption: Importers, development versions of Blender, modeling tools.
</tt>
</div>
</div>


===Complete report, but dependency cycle===
'''Example:''' [http://developer.blender.org/T95466 #95466]

'''Action:''' 
* Close the report. 
* Change the `Status >` label to `Status > '''Archived'''` (a bot should do this automatically - but only visible after a manual refresh of the page).
* Change the `Type >` label to `Type > '''Known Issue'''`


<div class="card bg-light">
<div class="card-body">
'''Message Example'''

<tt>
Undefined behavior of a rig when it has a dependency cycle is not a bug.
There is a chance it is a fake/avoidable dependency cycle, but these cases are mostly known and the solution for them requires bigger design reconsidrations which is outside of the scope of the bug tracker.
</tt>
</div>
</div>


===Unsupported graphics card or driver===

'''Action:''' 
* Change the `Status >` label to `Status > '''Needs Information from User'''`.


<div class="card bg-light">
<div class="card-body">
'''Need More Information'''

<tt>
Thanks for the report. This seems like a graphic driver issue. Please double-check if the drivers are up to date and the hardware meets Blender's requirements: https://www.blender.org/download/requirements

To upgrade to the latest driver, see here for more information:
https://docs.blender.org/manual/en/dev/troubleshooting/gpu/index.html
</tt>
</div>
</div>


'''Action:''' 
* Close the report. 
* Change the `Status >` label to `Status > '''Archived'''` (a bot should do this automatically - but only visible after a manual refresh of the page).


<div class="card bg-light">
<div class="card-body">
'''Old driver'''

<tt>
Thanks for the report. The graphics driver version is too old. Please upgrade to the latest driver, see here for more information:
https://docs.blender.org/manual/en/dev/troubleshooting/gpu/index.html
</tt>
</div>
</div>



<div class="card bg-light">
<div class="card-body">
'''Unsupported graphics card'''

<tt>
Thanks for the report. This GPU is below the minimum requirements for Blender, so we no longer provide support for it.
https://www.blender.org/download/requirements/

Installing the latest graphics driver sometimes helps to make such GPUs work, see here for more information.
https://docs.blender.org/manual/en/dev/troubleshooting/gpu/index.html

If that doesn't help, you can use Blender 2.79:
https://www.blender.org/download/previous-versions/
</tt>
</div>
</div>


===Blender doesn't run / crashes===

'''Example:''' [http://developer.blender.org/T77045 #77045]

'''Action:''' 
Unless the '''Worked''' field is properly filled, in which case we are talking about a valid regression:
* Close the report. 
* Change the `Status >` label to `Status > '''Archived'''` (a bot should do this automatically - but only visible after a manual refresh of the page).


<div class="card bg-light">
<div class="card-body">
'''Message Example'''

<tt>
Thanks for the report, but unfortunately this can't be triaged as a complete bug report.

For the development team to be able to help, there needs to be either reproducible steps (start blender, press this, then that and crash) or at least a known regression (Blender version X worked, but not version Y).

Many bugs are fixed between the official releases. Please try the latest development snapshot of blender: https://builder.blender.org/download
</tt>
</div>
</div>


== Need More Information Scenarios==
===Unexpected UI/Tool behaviour===
'''Example:''' [http://developer.blender.org/T68798 #68798]

'''Action:''' 
* Change the `Status >` label to `Status > '''Needs Information from User'''`
* Ask for more information from user.


<div class="card bg-light">
<div class="card-body">
'''Message Example'''

<tt>
Please try with File → Defaults →  Load Factory Settings to see if you still can reproduce this issue.
</tt>
</div>
</div>


===Complete report, but cannot reproduce===
'''Example:''' [http://developer.blender.org/T69415 #69415]

'''Note:''' Assumes triager tested both the latest stable Blender and the latest HEAD.

'''Actions:'''

* Change the `Status >` label to `Status > '''Needs Information from User'''`
* If user was not using the latest stable or a recent nightly build blender and ask for they to test with recent Blender;
* Otherwise ask for more clear instructions.


<div class="card bg-light">
<div class="card-body">
'''Message Example'''

<tt>
I cannot reproduce this with either the latest stable or current development versions of Blender:

Please try the latest daily build:
https://builder.blender.org/download/

* Go to File → Defaults → Load Factory Settings and then load your file to see if you still can reproduce this issue.

If the problem persists, please give us more clear instructions on how to reproduce it from scratch.
</tt>
</div>
</div>


=== Missing version of Blender that still worked ===

Knowing whether a problem was introduced recently or has been in Blender for years can help prioritising and triaging reports. For this the bug report template has a field "newest version of Blender that worked as expected". This field is often not filled out, or even removed by people.

'''Actions:'''

* Change the `Status >` label to `Status > '''Needs Information from User'''`
* Ask for testing with a few older versions of Blender.


<div class="card bg-light">
<div class="card-body">
'''Message Example'''

<tt>
Please test this with a few [older versions of Blender](https://download.blender.org/release/). Knowing whether a bug was recently introduced or has been in Blender for longer will help developers in finding the root cause of the issue.
</tt>
</div>
</div>


=== Crashes or errors involving your own C/C++ code (Python C/API for example) ===

'''Example:''' [http://developer.blender.org/T93622 #93622]

'''Actions:'''

* Change the `Status >` label to `Status > '''Needs Information from User'''`
* Ask for identifying the error in Blender's code.


<div class="card bg-light">
<div class="card-body">
'''Message Example'''

<tt>
Crashes or errors involving your own C/C++ code (Python C/API for example) will only be accepted as bugs if you are able to identify the error in Blender's code. Finding that your code isn't working as expected in Blender isn't sufficient to consider it a Blender bug as there may be other reasons that aren't necessarily caused by errors in Blender's code.
</tt>
</div>
</div>


==Valid Bug Reports==
Even valid confirmed reports can benefit from asking extra assistance from the reporter. In these cases (proven the files are not over complex) we can still go ahead and set their priorities.

Some of these topics have Advanced actions instructions. To follow these we presume the triager is already capable of building blender in release or debug modes.

===Valid report general instructions===
'''Action:'''
* Tag the corresponding module (change the `Module >` label). If unsure about their choice of module, CC the @blender/Module team which consists of the module owners.
* Change the `Status >` label to `Status > '''Confirmed'''`
* Check if the title is good otherwise edit it.
* If the file is complex enough that a developer would need to spend time further simplifying it, ask user to help.


<div class="card bg-light">
<div class="card-body">
'''Message Example'''

<tt>
Thanks for the report, I can confirm the issue. The sample file could still be simplified further. Could you find a way to nail it down to a face with one material, simplified ui, …?
</tt>
</div>
</div>


===Valid crash report, can confirm===
'''Example:''' [http://developer.blender.org/T69810 #69810], [http://developer.blender.org/T68586 #68586]

'''Note 1:''' Change the `Priority >` label to `Priority > '''High'''` unless it is a really obscure bug. If you think it should be '''Unbreak Now!''' contact the corresponding module developers or one of our coordinators.

'''Action:'''
* Tag the corresponding module (change the `Module >` label). If unsure about their choice of module, CC the @blender/Module team which consists of the module owners.
* Change the `Status >` label to `Status > '''Confirmed'''`
* Check if the title is good otherwise edit it.
* If the file is complex enough that a developer would need to spend time further simplifying it, ask user to help.

'''Advanced Actions:'''
* If possible also include a one line crash message with a full backtrace as inline code or text file
* If the issue was nailed down to a recent faulty commit, contact the commit author and/or subscribe the autor in the issue to raise awareness.


<div class="card bg-light">
<div class="card-body">
'''Message Example'''

<tt>
Note for developers:

BLI_assert failed: //source/blender/blenkernel/intern/object_update.c:429, BKE_object_eval_eval_base_flags(), at 'base_index < MEM_allocN_len(view_layer->object_bases_array) / sizeof(Base *)

Full backtrace: P1097
</tt>
</div>
</div>


===Valid crash report, can confirm it crashes "at random"===
'''Example:''' [http://developer.blender.org/T66862 #66862], [http://developer.blender.org/T67093 #67093]

'''Note:''' We need first to determine if it is a thread "run condition" or memory access issue.

'''Action:''' Run Blender with --debug-depsgraph-no-threads --threads 1 to see if crash still happens (can use a regular release Blender)


<div class="card bg-light">
<div class="card-body">
'''Message Example'''

<tt>
Thanks for the report.

Note for developers: I can confirm the issue with a regular Blender, but if I run it without threads (--debug-depsgraph-no-threads --threads 1) the issue goes away.
</tt>
</div>
</div>


'''Action:'''
* Tag the corresponding module (change the `Module >` label). If unsure about their choice of module, CC the @blender/Module team which consists of the module owners.
* Change the `Status >` label to `Status > '''Confirmed'''`
* Check if the title is good otherwise edit it.
* If the file is complex enough that a developer would need to spend time further simplifying it, ask user to help.

'''Advanced Action:''' Build a debug Blender with CMAKE_BUILD_TYPE=Debug WITH_COMPILER_ASAN=ON and WITH_ASSERT_ABORT=ON. If that still failed to catch the issue rebuild without the ASAN option, but with WITH_MEM_VALGRIND=ON instead. Include a one line error message with a full backtrace stored in https://developer.blender.org/paste (see example tasks).'''


<div class="card bg-light">
<div class="card-body">
'''Message Example'''

<tt>
SUMMARY: AddressSanitizer: heap-use-after-free //source/blender/editors/transform/transform_conversions.c:3669 in flushTransUVs

Full backtrace: P1039
</tt>
</div>
</div>


===Valid generic report, can confirm, known working version===
'''Example:''' [http://developer.blender.org/T54121 #54121]

'''Action:'''
* Tag the corresponding module (change the `Module >` label). If unsure about their choice of module, CC the @blender/Module team which consists of the module owners.
* Change the `Status >` label to `Status > '''Confirmed'''`
* Check if the title is good otherwise edit it.
* If the file is complex enough that a developer would need to spend time further simplifying it, ask user to help.
* If the issue was introduced within approximately the last 6 months this is a regression so change the `Priority >` label to `Priority > '''High'''`.

'''Advanced Action:''' 
* If the triager has the time, or user volunteers to help, give general bisect instructions. After bisect update the task description with the known problematic issue.
* Contact the commit author and/or subscribe the author in the issue to raise awareness.


<div class="card bg-light">
<div class="card-body">
'''Message Example'''

<tt>
Thanks for helping us looking into that. If you want to help further and you already know how to build a debug version of Blender, you can find the exact commit that caused this problem by:

# Try building blender with master, to make sure you have all the requirements to build blender The problem (bug) should be present in this build.
# Try building blender with version de7a8af79380 [known working version]. The problem (bug) should not be present in this build.
# Do a bisect with:

git bisect start

git bisect bad HEAD

git bisect good de7a8af79380

It usually anywhere from 7 to 10  steps. Which means you will need to build Blender approximately 10 times until you get the culprit. For questions and assistance with that, check #blender-coders on blender.chat and point anyone to this reply.
</tt>
</div>
</div>
