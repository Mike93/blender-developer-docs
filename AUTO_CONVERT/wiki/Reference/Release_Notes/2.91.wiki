= Blender 2.91 Release Notes =

Blender 2.91 was released on November 25, 2020.

Check out the final [https://www.blender.org/download/releases/2-91/ release notes on blender.org]. 

== [[Reference/Release Notes/2.91/User_Interface|User Interface]] ==

== [[Reference/Release Notes/2.91/Modeling|Modeling]] ==

== [[Reference/Release Notes/2.91/Sculpt|Sculpt]] ==

== [[Reference/Release Notes/2.91/Grease_Pencil|Grease Pencil]] ==

== [[Reference/Release Notes/2.91/Volumes|Volume Object]] ==

== [[Reference/Release Notes/2.91/EEVEE|EEVEE]] ==

== [[Reference/Release Notes/2.91/IO|I/O & Overrides]] ==

== [[Reference/Release Notes/2.91/Python_API|Python API]] ==

== [[Reference/Release Notes/2.91/Physics|Physics]] ==

== [[Reference/Release Notes/2.91/Animation-Rigging|Animation & Rigging]] ==

== [[Reference/Release Notes/2.91/Add-ons|Add-ons]] ==
== [[Reference/Release Notes/2.91/More_Features|More Features]] ==