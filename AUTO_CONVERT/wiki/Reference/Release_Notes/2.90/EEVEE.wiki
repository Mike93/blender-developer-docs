= Blender 2.90: EEVEE =

== Motion Blur Improvement ==

The motion blur has been done from scratch and now support mesh deformation, hair, and sub-frame accumulation for
even better precision. For more details [https://docs.blender.org/manual/en/dev/render/eevee/render_settings/motion_blur.html#example check out the manual]. ([https://projects.blender.org/blender/blender/commit/f84414d6e14c f84414d6e1] [https://projects.blender.org/blender/blender/commit/439b40e601f8 439b40e601])

[[File:Mb_32step_fx.png|thumb|600px|center]]

== Improvements ==

* Fix texture node repeat & filter mode to match Cycles better. ([https://projects.blender.org/blender/blender/commit/b2dcff4c21a6 b2dcff4c21])

== Sky Texture ==

* Port of Preetham and Hosek/Wilkie sky models. ([https://projects.blender.org/blender/blender/commit/9de09220fc5f 9de09220fc])

[[File:Hosek Eevee.png|thumb|600px|center]]