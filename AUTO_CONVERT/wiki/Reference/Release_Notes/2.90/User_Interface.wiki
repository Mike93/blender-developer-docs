= Blender 2.90: User Interface =

== Search ==

Operator search will now search through menu entries, to better show relevant operators and where to find them in menus. Adding operators to quick favorites or assigning shortcuts is now easy by right clicking on search results.

For developers that need more low-level search, Developer Extras can be enabled after which raw operators will appear in search results as well.  Add-ons that expose operators only through search [[Reference/Release_Notes/2.90/Python_API|need to be updated to expose them through a menu]].

== Layout ==

* Checkbox placements was reworked to place text on the right hand side, group together related settings and make layouts more compact. ([https://projects.blender.org/blender/blender/commit/219049bb3b76 219049bb3b], [https://projects.blender.org/blender/blender/commit/7fc60bff14a6 7fc60bff14])

[[File:Release notes properties checkbox split layout.png|thumb|center|500px]]
[[File:Release notes properties checkbox subpanel.png|thumb|center|500px]]

* Shader nodes in the material properties editor now show socket colors matching the shader editor, and are positioned on the left side. ([https://projects.blender.org/blender/blender/commit/7ef2dd84246c 7ef2dd8424], [https://projects.blender.org/blender/blender/commit/675d42dfc33d 675d42dfc3])<br/>

[[File:Release notes node socket icons.png|thumb|center|500px]]

== Statistics ==
* Scene statistics available in 3D viewport with an (optional) "Statistics" Viewport Overlay. ([https://projects.blender.org/blender/blender/commit/fd10ac9acaa0 fd10ac9aca]).
[[File:Statistics Overlay.png|thumb|center|Statistics Overlay|600px]]
* Status Bar now showing only version by default, but context menu can enable statistics, memory usage, etc. ([https://projects.blender.org/blender/blender/commit/c08d84748804 c08d847488]).
[[File:Status Bar Context Menu.png|thumb|center|Status Bar Context Menu|600px]]

== Modifiers ==
Modifiers and other stacks now support drag and drop for reordering. 

Their layouts were completely rewritten to make use of subpanels and the current user interface guidelines. 

Where they apply, the operation buttons ("Apply", "Copy" (now "Duplicate"), and "Apply as Shape Key") are now in a menu on the right side of the panel headers, along with new "Move to First" and "Move to Last" buttons.

<gallery mode="packed" widths=250px heights=550px perrow=4>
File:New-modifier-stack.png|Mesh Modifiers ({{GitCommit|9b099c86123f}})
File:Constraint UI 2.90.png|Constraints ({{GitCommit|eaa44afe703e}})
</gallery>

There are now shortcuts for common modifier operations. ([https://projects.blender.org/blender/blender/commit/1fa40c9f8a81 1fa40c9f8a]) Enabled by default are: 
* Remove: X, Delete
* Apply: Ctrl A
* Duplicate: Shift D

== About ==

* About Blender dialog added to the App menu to contain information like build, branch, and hash values. This declutters the Splash Screen and highlights only the release version. ([https://projects.blender.org/blender/blender/commit/5b86fe6f3350 5b86fe6f33]). 
[[File:About18.png|thumb|center|600px]]

<br style="clear:both;">

== Miscellaneous ==

* Area borders given much larger hit area to make them easier to resize. ([https://projects.blender.org/blender/blender/commit/4e8693ffcddb 4e8693ffcd])
[[File:Border Hit Size.png|thumb|center|Increased Border Hit Area|600px]]
* Support for Windows Shell Links (shortcuts) in File Browser. Extended Mac Alias usage. Better display of linked items. ([https://projects.blender.org/blender/blender/commit/1f223b9a1fce 1f223b9a1f])
[[File:Shortcuts.png|thumb|center|Shortcuts and Aliases|600px]]
* Pie menus support for A-Z accelerator keys, for quickly selecting items with the keyboard.
* New "Instance Empty Size" setting in user preferences ([https://projects.blender.org/blender/blender/commit/e0b5a202311 e0b5a20231]).
* Text field buttons now support local undo and redo while being edited. ([https://projects.blender.org/blender/blender/commit/1e12468b84d2 1e12468b84]).
* Outliner Delete operator added to delete all selected objects and collections using either the keymap or context menu. ([https://projects.blender.org/blender/blender/commit/ae98a033c856 ae98a033c8]). Replaces the Delete Object and Delete Collection operators.
* Outliner Delete Hierarchy now operates on all selected objects and collections. Additionally, the menu option is shown for objects in the View Layer display mode. ([https://projects.blender.org/blender/blender/commit/26c0ca3aa7f4 26c0ca3aa7]).
* Dragging outliner elements to an edge will scroll the view. ([https://projects.blender.org/blender/blender/commit/7dbfc864e6f8 7dbfc864e6])
* Dragging panels to the edge of a region will scroll it up or down. ([https://projects.blender.org/blender/blender/commit/859505a3dae8 859505a3da])
* Move a few FFmpeg render properties from the scene properties to the audio output settings. ([https://projects.blender.org/blender/blender/commit/a55eac5107ed a55eac5107])
* Update the terminology used in some properties. ([https://projects.blender.org/blender/blender/commit/1278657cc2d 1278657cc2]), ([https://projects.blender.org/blender/blender/commit/3ea302cf8ef 3ea302cf8e]), ([https://projects.blender.org/blender/blender/commit/ef0ec014611 ef0ec01461])
* 3D Viewport: Add Edge Loopcut Slide to edge menu. ([https://projects.blender.org/blender/blender/commit/0fdb79fe584 0fdb79fe58])
* The interface for many operators was updated to be clearer and more consistent with the rest of Blender. ([https://projects.blender.org/blender/blender/commit/17ebbdf1c17d 17ebbdf1c1])
* Option to print text in bold or italics style, synthesized from a single base UI font. ([https://projects.blender.org/blender/blender/commit/b74cc23dc478 b74cc23dc4])