= Blender 2.90: Import & Export =

== Alembic ==

* Matrices with negative scale can now be properly interpolated. This means that it is now possible to take an Alembic file that was saved at 30 FPS and load it into a 24 FPS Blender file, even when there are mirrored objects. ([https://projects.blender.org/blender/blender/commit/a5e176a8ed8a a5e176a8ed] and demo videos in [https://developer.blender.org/D8048 D8048]).
* Object data is now exported with the object data name ([https://projects.blender.org/blender/blender/commit/0d744cf673e 0d744cf673]). Previously the Alembic exporter exported a mesh object to `{object.name}/{object.name}Shape`. Now it exports to `{object.name}/{mesh.name}` instead. The same change also applies to other object data types.
* Spaces, periods, and colons in the names of particle systems are now replaced with underscores ([https://projects.blender.org/blender/blender/commit/f106369ce85 f106369ce8]). Other types already had spaces, periods, and colons replaced by underscores, and now particle systems are exported with the same naming convention.
* Blender now always exports transforms as as "inheriting", as Blender has no concept of parenting without inheriting the transform ([https://projects.blender.org/blender/blender/commit/2dff08c8ce9 2dff08c8ce]). Previously only objects with an actual parent were marked as "inheriting", and parentless objects as "non-inheriting". However, certain packages (for example USD's Alembic plugin) are incompatible with non-inheriting transforms and will completely ignore such transforms, placing all such objects at the world origin.
* Blender now exports Alembic using the same approach as USD. This means that Alembic and USD files are more consistent, and that it's easier to solve (or even prevent) certain bugs ([https://projects.blender.org/blender/blender/commit/2917df21adc 2917df21ad]). Changes are:
** Duplicated objects now have a unique numerical suffix (also see above).
** Matrices are computed differently. This fixes T71395, but otherwise should produce the same result as before (but with simpler, more predictable code).
* Alembic's obsolete HDF5 compression format has never been officially supported by Blender, but only existed as an optional build-time option that was disabled for all Blender releases. The support for HDF5 has now been completely removed ([https://projects.blender.org/blender/blender/commit/0c3843622726 0c38436227], [https://projects.blender.org/blender/blender/commit/0102b9d47edf 0102b9d47e]).

== USD ==

* Hair particle systems are now exported using the Particle System name. Previously this was done with the Particle Settings name. This gives more control over the final name used in USD and is consistent with the Alembic exporter. ([https://projects.blender.org/blender/blender/commit/fc0842593f0 fc0842593f]).

== Instances ==

USD and Alembic export can now correctly export nested instances. Further, numbers used to identify duplicated (i.e. instanced) objects have been reversed ([https://projects.blender.org/blender/blender/commit/98bee41c8a3f78c 98bee41c8a]).

This produces hierarchies like this:
    
      Triangle
        |--TriangleMesh
        |--Empty-1
        |    +--Pole-1-0
        |        |--Pole
        |        +--Block-1-1
        |            +--Block
        |--Empty
        |    +--Pole-0
        |        |--Pole
        |        +--Block-1
        |            +--Block
        |--Empty-2
        |    +--Pole-2-0
        |        |--Pole
        |        +--Block-2-1
        |            +--Block
        +--Empty-0
            +--Pole-0-0
                |--Pole
                +--Block-0-1
                    +--Block

It is now clearer that `Pole-2-0` and `Block-2-1` are instanced by `Empty-2`. Before Blender 2.90 they would have been named `Pole-0-2` and
`Block-1-2`.

== glTF 2.0 ==

* Importer
** Enhancements
*** Implement KHR_materials_clearcoat ([https://projects.blender.org/blender/blender-addons/commit/64d34396670d  rBA64d3439])
*** Implement KHR_mesh_quantization ([https://projects.blender.org/blender/blender-addons/commit/4b656b65f81d  rBA4b656b6])
*** Improve the layout of nodes in the material graph ([https://projects.blender.org/blender/blender-addons/commit/5b4ed4e574ab  rBA5b4ed4e])
*** Add option to glue pieces of a mesh together ([https://projects.blender.org/blender/blender-addons/commit/c7eda7cb49f7  rBAc7eda7c])
*** Code cleanup & refactoring & performance ([https://projects.blender.org/blender/blender-addons/commit/bb4dc6f1daab  rBAbb4dc6f], [https://projects.blender.org/blender/blender-addons/commit/9fd05ef46664  rBA9fd05ef])
** Fixes
*** Fix occlusion textures ([https://projects.blender.org/blender/blender-addons/commit/a29e15e11ed2  rBAa29e15e])
*** Fix Crash on undo after glTF import ([https://projects.blender.org/blender/blender-addons/commit/d777821fd6ad  rBAd777821])
* Exporter
** Enhancements
*** Add joint / pre /post in hook for extensions ([https://projects.blender.org/blender/blender-addons/commit/e3bb132d1f68  rBAe3bb132], [https://projects.blender.org/blender/blender-addons/commit/40db41a902be  rBA40db41a])
*** Allow combining different-sized textures (eg for ORM) ([https://projects.blender.org/blender/blender-addons/commit/09508f2dcf2a  rBA09508f2])
*** Add check rotation + delta rotation both animated ([https://projects.blender.org/blender/blender-addons/commit/ee2a0831d8c1  rBAee2a083])
*** Code clean up & refactoring & performance ([https://projects.blender.org/blender/blender-addons/commit/2d8c1b2c6184  rBA2d8c1b2], [https://projects.blender.org/blender/blender-addons/commit/01186b0df9c5  rBA01186b0], [https://projects.blender.org/blender/blender-addons/commit/3ea1673580ab  rBA3ea1673], [https://projects.blender.org/blender/blender-addons/commit/9313b3a155bb  rBA9313b3a], [https://projects.blender.org/blender/blender-addons/commit/03e3ef7f71f2  rBA03e3ef7])
*** Refactoring Normals export ([https://projects.blender.org/blender/blender-addons/commit/bd8e1f3e576f  rBAbd8e1f3], [https://projects.blender.org/blender/blender-addons/commit/422c47c5f79e  rBA422c47c], [https://projects.blender.org/blender/blender-addons/commit/52f88967a6e7  rBA52f8896])
*** Add support for use_inherit_rotation and inherit_scale ([https://projects.blender.org/blender/blender-addons/commit/63dd8498ac10  rBA63dd849])
*** Export curve/surface/text objects as meshes ([https://projects.blender.org/blender/blender-addons/commit/47ea656bdd61  rBA47ea656])
** Fixes
*** Prevent infinite recursion when mesh is skinned and parenting to same bone ([https://projects.blender.org/blender/blender-addons/commit/72227fc13ba3  rBA72227fc])
*** Make sure rotation are normalized ([https://projects.blender.org/blender/blender-addons/commit/fac4c6443ba7  rBAfac4c64])
*** Add check when armature animation is binded to mesh object ([https://projects.blender.org/blender/blender-addons/commit/61f7f5f3a57b  rBA61f7f5f])
*** Fix extension panel appearance ([https://projects.blender.org/blender/blender-addons/commit/aea05413b768  rBAaea0541])
*** Fix draco UV export ([https://projects.blender.org/blender/blender-addons/commit/e47d2bcfad93  rBAe47d2bc])
*** Fix lamp parented to bone ([https://projects.blender.org/blender/blender-addons/commit/1d29fc5b9161  rBA1d29fc5])
*** Fix saving use_selection option in .blend file ([https://projects.blender.org/blender/blender-addons/commit/7cbb383d2213  rBA7cbb383])
*** Fix exporting with option "group by NLA" ([https://projects.blender.org/blender/blender-addons/commit/7a0a9182c82b  rBA7a0a918])
*** Fix exporting EXTEND textures ([https://projects.blender.org/blender/blender-addons/commit/cbad9300d7e1  rBAcbad930])
*** Fix export alpha scalar value (not coming from texture) ([https://projects.blender.org/blender/blender-addons/commit/48c8d6c23010  rBA48c8d6c])
*** Fix exporting `aspectRatio` for Perspective Cameras ([https://projects.blender.org/blender/blender-addons/commit/2b4bf943d0a3  rBA2b4bf94])
*** Fix to generate valid file when zero-weight verts ([https://projects.blender.org/blender/blender-addons/commit/386bb5eaa473  rBA386bb5e])