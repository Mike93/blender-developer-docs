= Blender 2.81: Add-ons =

== Add-on Preferences ==

* A checkbox "Enabled Add-ons Only" was added to the add-on list. When checked, only the enabled add-ons are shown; un-check to show all installed add-ons. This replaces the 'Enabled' and 'Disabled' items in the categories filter. [https://projects.blender.org/blender/blender/commit/078d02f55743cd34c5 078d02f557]

== Add-on Preferences: New Add-on Category List ==

* During the transition from 2.80 to 2.81, there has been significant changes to Add-ons and the categories they appear in. Some of the addons may have moved to a new category. The 3D View category has been limited to operations on the 3D Viewport such as overlays and a new category "Interface" created. Interface deals with addons that are UI based. The Pie menus addon has been moved to Interface. The category list is refined in other areas to better group Add-ons and reduce ui clutter.

== Add-on Tabs and Panels ==

* Most Add-ons included in Blender 2.81 have their panels closed by default to avoid ui clutter. We have also changed the tab naming conventions to better integrate addons into the ui. A decision was made that add-ons should not use the Active Tool tab unless it's a modal operator. 
* Add-ons have been grouped into the Item, View and custom Edit, Create and Animate tabs with the exceptions of a few use specific addons using custom names. If you enable all addons in order with the exception of the custom names, then enable the custom names, switching between object modes has a significantly reduced Tab "jumping" when using addons.
https://developer.blender.org/T70017 

== glTF 2.0 IO ==

=== Importer ===

* Meshes
** manage mode other than 4 (manage edges & points without faces) [https://projects.blender.org/blender/blender-addons/commit/7565f120d54c  rBA7565f12]
** Big performance improvment [https://projects.blender.org/blender/blender-addons/commit/06bb353c8489  rBA06bb353], [https://projects.blender.org/blender/blender-addons/commit/a0c33fe957a9  rBAa0c33fe], [https://projects.blender.org/blender/blender-addons/commit/ba3c5b0b3b82  rBAba3c5b0], [https://projects.blender.org/blender/blender-addons/commit/96036641899e  rBA9603664], [https://projects.blender.org/blender/blender-addons/commit/eba6fe23d3bb  rBAeba6fe2], [https://projects.blender.org/blender/blender-addons/commit/f9e25350dc87  rBAf9e2535]
* Animation
** better multi object animation management [https://projects.blender.org/blender/blender-addons/commit/87ffe55fd5f7  rBA87ffe55]

=== Exporter ===

* Materials
** Check that textures have valid image [https://projects.blender.org/blender/blender-addons/commit/2c08beb9690c  rBA2c08beb]
** Add alpha support [https://projects.blender.org/blender/blender-addons/commit/1e20236039c8  rBA1e20236], alpha support in vertex color [https://projects.blender.org/blender/blender-addons/commit/26c53aa581b0  rBA26c53aa]
** Normal export fix when there are normal modifier(s) [https://projects.blender.org/blender/blender-addons/commit/3c3c2243dbbd  rBA3c3c224]
** Performance [https://projects.blender.org/blender/blender-addons/commit/a9283e526fea  rBAa9283e5]
** Better texture transform mapping management [https://projects.blender.org/blender/blender-addons/commit/42f1e69458db  rBA42f1e69], [https://projects.blender.org/blender/blender-addons/commit/d8e78e3cdcd2  rBAd8e78e3], [https://projects.blender.org/blender/blender-addons/commit/8953c208ad1b  rBA8953c20]
* Animation
** Sample animations is now default to True [https://projects.blender.org/blender/blender-addons/commit/c402af6e27ca  rBAc402af6]
** Fix skinning export [https://projects.blender.org/blender/blender-addons/commit/7307a3c57d84  rBA7307a3c]
** Fix first and last tangent data for bezier interpolation [https://projects.blender.org/blender/blender-addons/commit/0f85dace7664  rBA0f85dac]
** Take into account NLA tracks for shapekeys animation [https://projects.blender.org/blender/blender-addons/commit/7f732e373a23  rBA7f732e3]
** Various sanity checks [https://projects.blender.org/blender/blender-addons/commit/72599842ab5b  rBA7259984], [https://projects.blender.org/blender/blender-addons/commit/5db869653a60  rBA5db8696], [https://projects.blender.org/blender/blender-addons/commit/ffcecad3a890  rBAffcecad], [https://projects.blender.org/blender/blender-addons/commit/46b1ada7f5f2  rBA46b1ada], [https://projects.blender.org/blender/blender-addons/commit/4e69d972bd4f  rBA4e69d97], [https://projects.blender.org/blender/blender-addons/commit/525afbadafbe  rBA525afba]
** Fix ShapeKeys animation export [https://projects.blender.org/blender/blender-addons/commit/fc320ea236c7  rBAfc320ea], [https://projects.blender.org/blender/blender-addons/commit/dcd48a616b22  rBAdcd48a6], [https://projects.blender.org/blender/blender-addons/commit/154758b5a3d8  rBA154758b]
** Better multiple animation export (including merging animations) [https://projects.blender.org/blender/blender-addons/commit/9a7d0db875af  rBA9a7d0db], [https://projects.blender.org/blender/blender-addons/commit/14f1c99e96ad  rBA14f1c99]
** Performance [https://projects.blender.org/blender/blender-addons/commit/8d9e3c94aff9  rBA8d9e3c9], [https://projects.blender.org/blender/blender-addons/commit/812cb318c4e0  rBA812cb31]
* Object
** export object from linked library [https://projects.blender.org/blender/blender-addons/commit/ade13102347c  rBAade1310]
** Export custom light ranges [https://projects.blender.org/blender/blender-addons/commit/c49ac6dee0e7  rBAc49ac6d]
* General
** Better blender version handling [https://projects.blender.org/blender/blender-addons/commit/50394a12df3f  rBA50394a1], [https://projects.blender.org/blender/blender-addons/commit/54e504f32b1a  rBA54e504f]
** Set main scene [https://projects.blender.org/blender/blender-addons/commit/ea5f0df783b6  rBAea5f0df]
** UI change [https://projects.blender.org/blender/blender-addons/commit/12af8a28c14b  rBA12af8a2]

== FBX IO ==

* Crease and sub-surface informations are now supported on both exporting and importing ([http://developer.blender.org/D4982 D4982], [https://projects.blender.org/blender/blender-addons/commit/f1dd37b8ac8f  rBAf1dd37b]).
* Importer can now batch-import several FBX files at once ([http://developer.blender.org/D5866 D5866], [https://projects.blender.org/blender/blender-addons/commit/b57772a8831e  rBAb57772a]).
* After the change to the shader's mapping node ([https://projects.blender.org/blender/blender/commit/baaa89a0bc5  rBbaaa89a0]), the min/max feature being removed, the handling of U/V clamping of FBX textures has been modified in the importer. It now follows the same behavior as the exporter, FBX's individual clamping/repeat on the U or V axes are not supported anymore (if any one of those is set, Blender's Texture node Extension is set to 'repeat').

== Import Images as Planes ==

* The default material shader now uses the Principled node, which allows to also automatically support transparency, and exports directly in several IO formats (GLtf, OBJ, FBX, ..., [http://developer.blender.org/D5610 D5610], [https://projects.blender.org/blender/blender-addons/commit/a215a3c85ad9  rBAa215a3c]).

== BlenderKit ==

* A right click menu in the asset bar offers several new (and old) functions :
** Search similar - searches for similar assets
** Swap model (only for models) - swaps the selection for the new assets
** Delete (own assets). 
* Upload has been improved, with possibility to upload thumbnail and main file separately
* More UI options, mainly thumbnail size
* The code of the addon now runs on timers, which enables it to append/link assets also when the asset bar is hidden.

== Rigify ==

A big internal API refactoring that has been worked on since last year has been committed. ([https://projects.blender.org/blender/blender-addons/commit/3423174b37  rBA3423174], [https://projects.blender.org/blender/blender-addons/commit/8b1df84370  rBA8b1df84], etc)

=== For users ===

* Refactored rigs generate nearly instantly compared to before. Note that the face rig has not been updated because it is planned to be replaced in the future, and is still slow.
* The Rigify Animation Tools panel has been deprecated and disabled for newly generated rigs. Buttons for updating action keyframes have been moved to the rig-specific Rig Main Properties panel, next to the original IK<->FK snap buttons.
* IK controls have much more sophisticated support for switching the active parent: choices between root, torso, hip, etc; operators to change parent without moving control, etc.
* The super_spine rig has been split into separate head, middle and tail parts; the old combined spine is now just a compatibility wrapper.

=== For scripters ===

* Rig classes have a [[Process/Addons/Rigify|new API]] for interacting with Rigify core, designed to support complex rig interaction and subclassing.
* Old rig classes still work via an automatic compatibility wrapper, except if they rely on being in Edit mode inside `__init__`.
* New utilities for generating keyframe bake operators specific to the rig in the rig UI script, instead of the old Rigify Animation Tools panel hardcoded in the add-on to deal with the 'standard' metarig and nothing else.
* A number of other new small utilities in the library.

These changes were designed to allow taking full advantage of the capability to implement and install custom rigs (available since 2.80) via feature set packages ([https://github.com/angavrilov/angavrilov-rigs/ feature set example], [https://github.com/angavrilov/rigify-legacy-rigs legacy rig feature set]).

== Pie Menu Add-ons ==

* As Blender 2.8 brought about many pie menus as default menus, the "Official" pie menu addon has been depreciated. The Viewport Pies add-on is still available as an alternate pie menu system. It has also been updated to respect the Workspace Filter Add-ons and several individual menu fixes. You can now find it in the interface category when you enable addons.

== Dynamic Context Menu ==

* The dynamic context or "spacebar" addon has had a rework for the 2.8 series. Originally designed as as a replacement for the legacy 2.49 spacebar menu, then updated up to 2.79 release, this addon had evolved again to adjust to the new Blender tools and interface.
* Access Search, Tools, Animation, Quick Favorites in the one place. Collections added. Menu's are reworked and updated to better reflect built in 2.8 series tools.
* Note: If you have trouble accessing the add-on in the ui, in Preferences > Keymaps > Spacebar Action, you may need to set it to tools and save your preferences.

== Materials Utils ==

* Updated from 2.79, the Materials Utils addon makes a return in 2.81. Access your materials in the 3d viewport and assign, search, select, copy and more.

== Animall ==

* Updated from 2.79, the Animall addon makes a return in 2.81 thanks to Damien Picard (pioverfour). animate mesh, lattice and curve data.