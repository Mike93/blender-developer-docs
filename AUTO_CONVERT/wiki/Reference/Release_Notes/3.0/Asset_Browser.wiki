= Blender 3.0: Asset Browser =

[[File:Release notes asset browser banner.png|640px|right]]

Blender 3.0 includes a new editor: The Asset Browser. It makes working with reusable assets much easier and more visual. Supported asset types in 3.0 are:
* Objects
* Materials
* Poses
* Worlds
More asset types will be supported in future.

<br style="clear:both;">

== Quick Start ==

The following gives a quick overview on how to use asset functionality.

=== Regular Data-Blocks ===

For objects, materials and worlds, the following sections explain the workflow. For pose assets, the workflow differs a bit and is explained below.

==== Creating Assets ====

The ''Mark as Asset'' operator makes data-blocks available for easy reuse via the Asset Browser. It can be found in multiple places:
* Context menu [https://docs.blender.org/manual/en/latest/editors/outliner/editing.html#context-menu in the Outliner]. Allows marking multiple data-blocks as assets at once.
* Context menu of the [https://docs.blender.org/manual/en/latest/render/materials/assignment.html#material-slots material slots] list.
* For objects in the 3D View: ''Object'' > '' Asset'' > ''Mark as Asset''.

'''Save the file!''' Otherwise your asset edits won't be available to other files.

==== Register an Asset Library ====

[[File:Release notes asset browser library preferences.png|thumb]]

Save the file into a directory that you want to use as your asset library. This directory then has to be registered as an asset library in the Preferences: ''Preferences'' > ''File Paths'' > ''Asset Libraries''.

<br style="clear:both;">

==== Using Assets ====

Assets can simply be dragged into the 3D View, and in some instances, into other editors. Objects will snap to the surface under the cursor or to the grid. Materials will be applied to the material slot under the cursor.


==== Editing Asset Metadata ====

[[File:Release_notes_asset_browser_sidebar.png|right|340px]]


The Asset Browser has an ''Asset Details'' sidebar in which the metadata of assets can be edited '''if the asset is stored in the current file'''. Such assets are displayed with a special icon: 

[[File:Release notes asset browser current file icon.png|Icon indicating that the asset is stored in the current .blend file and can be edited.]]

<br style="clear:both;">

=== Poses ===

The Pose Library system has been revamped to make use of the new Asset Browser. [https://docs.blender.org/manual/en/3.0/animation/armatures/posing/editing/pose_library.html Read more in the manual].

[[File:Release notes asset browser poses.png|640px|right]]


<br style="clear:both;">

== More Documentation ==

All Asset Browser related functionality is documented in the [https://docs.blender.org/manual/en/latest/index.html manual].

* [https://docs.blender.org/manual/en/latest/editors/asset_browser.html Asset Browser]
* [https://docs.blender.org/manual/en/latest/files/asset_libraries/index.html Asset Libraries and Asset Catalogs]
* [https://docs.blender.org/manual/en/latest/editors/preferences/file_paths.html#asset-libraries Asset Library Registration]
* [https://docs.blender.org/manual/en/latest/animation/armatures/posing/editing/pose_library.html Pose Library] (based on the Asset Browser)



