= Blender 3.0: Pipeline, Assets & IO =

== Packing ==

* Packing linked libraries is now accessible via the File menu ([https://projects.blender.org/blender/blender/commit/ee51e7335552  rBee51e733]).
* Cleanup of the other External Data options ([https://projects.blender.org/blender/blender/commit/ee51e7335552  rBee51e733]).

[[File:External Data Packed.png]]

== Alembic ==

* Animated UV maps are now exported to Alembic ([https://projects.blender.org/blender/blender/commit/3e77f747c0d1  rB3e77f747]).
* Generated mesh vertex coordinates (also known as ORCOs) are now exported to and imported from Alembic ([https://projects.blender.org/blender/blender/commit/f9567f6c63e7  rBf9567f6c]). They're stored in `.arbGeomParams/Pref` in the Alembic file.
* Per vertex UV maps are now imported from Alembic ([https://projects.blender.org/blender/blender/commit/3385c04598f 3385c04598]). Such UV maps can be defined by other software to reduce file size when the mesh is split according to UV islands. Blender, however, still stores the UV data per face corner.
* Non functional "Renderable Objects only" option has been removed ([https://projects.blender.org/blender/blender/commit/834e87af7bbf 834e87af7b]), has been superseeded by the following:
* New option to set evaluation mode to Render or Viewport (for visibility, modifiers) in [https://projects.blender.org/blender/blender/commit/8f5a4a245388 8f5a4a2453]
* New option to always add a cache reader when importing files ([https://projects.blender.org/blender/blender/commit/5b97c00e9fc4e 5b97c00e9f]). This simplifies workflows for updating objects after imports if they change in the cache.

== USD Importer ==

USD files can now be imported into Blender ([https://projects.blender.org/blender/blender/commit/ea54cbe1b42e  rBea54cbe1]). The USD importer works in a similar fashion as the Alembic importer.

Learn more about Importing USD Files in the [https://docs.blender.org/manual/en/3.0/files/import_export/usd.html#importing-usd-files Blender manual].

== glTF 2.0 ==

=== Importer ===
* Import custom properties from default scene ([https://projects.blender.org/blender/blender-addons/commit/260ca332f88f643a302bcabf140e3c471c8c621b  rBA260ca33])
* Fix issue involving flipped bone Z dir ([https://projects.blender.org/blender/blender-addons/commit/b986a52051d48c2cda5a38159d05fb198cc15db7  rBAb986a52])
* Avoid traceback when trying to import some invalid glTF 2.0 files ([https://projects.blender.org/blender/blender-addons/commit/4d562682c099ce740d705893008a91a30a206710  rBA4d56268])

=== Exporter ===
* Add option to keep original texture files ([https://projects.blender.org/blender/blender-addons/commit/0cdaac6f9a3e318b1d5db04ade2838d004cd500d  rBA0cdaac6])
* Do not export glTF internal settings as extras ([https://projects.blender.org/blender/blender-addons/commit/57ef445314fef1290b155cb182ee7e513bc22008  rBA57ef445])
* Remove some channel animation if bone is not animated ([https://projects.blender.org/blender/blender-addons/commit/1757ec91e58a23c6dbd31b763267e1d3f5e40026  rBA1757ec9], [https://projects.blender.org/blender/blender-addons/commit/a8c700d4ebe6a1a767758a0b79775d48d7fbe085  rBAa8c700d], [https://projects.blender.org/blender/blender-addons/commit/45132ba0f4410e9b7126bdc0a141ffca8b801c8a  rBA45132ba])
* Draco: more explicit error message ([https://projects.blender.org/blender/blender-addons/commit/eedbe303799fb8af4e7dafea81a0dd46973fb85b  rBAeedbe30])
* Fix bug when invalid shapekey driver ([https://projects.blender.org/blender/blender-addons/commit/ac6fe2ff7c9b68b8a08e74ac3b8e350a6eb9d24d  rBAac6fe2f], [https://projects.blender.org/blender/blender-addons/commit/cd7092335690c5644c5c75cea23672510fd58ce3  rBAcd70923])
* Fix driver export when shapekey as a dot in name ([https://projects.blender.org/blender/blender-addons/commit/54d8bdf1f15597b9f6e1aaf308b5a0d5df785552  rBA54d8bdf])
* Fix default date type at numpy array creation ([https://projects.blender.org/blender/blender-addons/commit/97cf91034d0d5255269a0b8c506f2cdc8e81b789  rBA97cf910])
* Make sure that addon that changes root gltf are taken into account ([https://projects.blender.org/blender/blender-addons/commit/0aa618c849ffef7b11cef81f1712b89e0b0e337b  rBA0aa618c])
* Don't erase empty dict extras ([https://projects.blender.org/blender/blender-addons/commit/6c1a227b3fa329d56bdaf0e4ebd6d72aa92d2bfe  rBA6c1a227])
* Add merge_animation_extensions_hook ([https://projects.blender.org/blender/blender-addons/commit/669b89d6a56dfebee45c4b72950d212da04d17df  rBA669b89d])
* Do not export empty node when camera or light export is disable ([https://projects.blender.org/blender/blender-addons/commit/060d0e76fbb2319e12e8fdf8435debe5267bfc7d  rBA060d0e7])
* Fix crash when trying to export some muted driver(s) ([https://projects.blender.org/blender/blender-addons/commit/902b8ba11efa98006b3a247e4549ca69fb458309  rBA902b8ba])
* Change order of format option ([https://projects.blender.org/blender/blender-addons/commit/760b5d3a46416bd55dabec09aa38385ac3ec2467  rBA760b5d3])
* Cleanup: use inverted_safe on matrices ([https://projects.blender.org/blender/blender-addons/commit/29c9aa34754494bc478a08f0be60f38f4844a83d  rBA29c9aa3])
* Validate meshes before export ([https://projects.blender.org/blender/blender-addons/commit/5bffedce241f04dc57374ea86f9701e1fddff47f  rBA5bffedc])
* Better 'selection only' management ([https://projects.blender.org/blender/blender-addons/commit/e7f22134350127ac18747c367bb0ad9a1ef2d8a3  rBAe7f2213])
* Fix animation export for objects parented to bones ([https://projects.blender.org/blender/blender-addons/commit/8975ba0a553a6cbe6b9e086a474331bddfb55841  rBA8975ba0])
* Cleanup object animation curves when animation is constant ([https://projects.blender.org/blender/blender-addons/commit/ba969e8b536781450d8b43959c95ca07e886b2b6  rBAba969e8])
* Change after change on vertex groups data now on mesh ([https://projects.blender.org/blender/blender-addons/commit/64fcec250d2d9450ac64d8314af7092a6922a28b  rBA64fcec2])
* Avoid issue with setting frame with python v >= 3.10 ([https://projects.blender.org/blender/blender-addons/commit/a0d1647839180388805be150794a812c44e59053  rBAa0d1647])