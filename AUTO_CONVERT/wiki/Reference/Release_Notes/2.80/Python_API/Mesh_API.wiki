= Blender 2.80: Mesh API =

== Mesh Tesselation ==

The legacy Blender tesselated faces have been removed from the API. They were a leftover from before n-gon support was added and only supported triangles and quads.

For most cases, mesh polygons should be used instead. Exporters and renderers that need to tesselate the mesh can use ''loop triangles'' as a replacement.

'''2.7x'''
<syntaxhighlight lang="Python">
mesh.calc_tessface()

for face in mesh.tessfaces:
    for vert_index in face.vertices:
        print(mesh.vertices[vert_index].co)
</syntaxhighlight>

'''2.8x'''
<syntaxhighlight lang="Python">
mesh.calc_loop_triangles()

for tri in mesh.loop_triangles:
    for vert_index in tri.vertices:
        print(mesh.vertices[vert_index].co)
</syntaxhighlight>

Loop triangles tessellate the mesh into triangles only. If quads are required, consecutive loop triangles corresponding to the same polygon can be considered as one quad.

UVs and vertex colors no longer have their own arrays, instead the loop indices can be used for access consistent with polygons.

'''2.7x'''
<syntaxhighlight lang="Python">
for uv_layer in mesh.tessface_uv_textures:
    for face in mesh.tessfaces:
        face_uvs = uv_layer.data[face.index]
        for uv in face_uvs:
            print(uv)
</syntaxhighlight>

'''2.8x'''
<syntaxhighlight lang="Python">
for uv_layer in mesh.uv_layers:
    for tri in mesh.loop_triangles:
        for loop_index in tri.loops:
            print(uv_layer.data[loop_index].uv)
</syntaxhighlight>

'''Tip:''' ''mesh.uv_textures'' has been removed

== BMesh Module ==

==== Operator Enumerators and Flags ====

BMesh operator enumerators/flags in 2.7x were integer arguments. In 2.8x this has been changed to string enumerators:

'''2.7x'''
<syntaxhighlight lang="Python">
bmesh.ops.mirror(bm, geom=verts, axis=1)
</syntaxhighlight>

'''2.8x'''
<syntaxhighlight lang="Python">
bmesh.ops.mirror(bm, geom=verts, axis='Y')
</syntaxhighlight>

== Skin Root Vertices ==

Mesh skin vertex roots can now be set through the API.

<syntaxhighlight lang="Python">
l = bm.verts.layers.skin.verify()
v = bm.verts[0]
v[l].use_root = True
</syntaxhighlight>

{|class="note"
|-
|<div class="note_title">''''''</div>
<div class="note_content">Each mesh island should only have one root vertex, however it is up to script author to ensure this is the case.</div>
|}
