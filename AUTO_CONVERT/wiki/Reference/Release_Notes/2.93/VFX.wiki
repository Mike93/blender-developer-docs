= Blender 2.93: VFX & Video =

== FFmpeg ==
* Improve scrubbing performance ([https://projects.blender.org/blender/blender/commit/88604b79b7d1 88604b79b7])
* Improve proxy building performance ([https://projects.blender.org/blender/blender/commit/e4f347783320 e4f3477833])
* Improve threading settings ([https://projects.blender.org/blender/blender/commit/1614795ae2bf 1614795ae2])
* Adjust default proxy settings ([https://projects.blender.org/blender/blender/commit/b9207fb43d6f b9207fb43d])

== Motion tracking ==

* Add track averaging operator ([https://projects.blender.org/blender/blender/commit/f17b26bbed3 f17b26bbed])
* Resolved old-standing issue with Lock-to-View causing jumps when changing selection ([https://projects.blender.org/blender/blender/commit/66f8835f9c03 66f8835f9c]). The fix also avoids jump when toggling lock-to-selection option.

== Sequencer ==

* Inherit blend mode with single input effects ([https://projects.blender.org/blender/blender/commit/d6bbcc4f666b d6bbcc4f66])
* Automatic proxy building ([https://projects.blender.org/blender/blender/commit/04e1feb83051  rB04e1feb8])
* Simplify proxy settings ([https://projects.blender.org/blender/blender/commit/91561629cd0b  rB91561629])
* Preview images when moving strip handles ([https://projects.blender.org/blender/blender/commit/3d9ee83d8818  rB3d9ee83d])
* Text strip improvements ([https://projects.blender.org/blender/blender/commit/1c095203c275  rB1c095203])
* Add bold and italic option for text strip ([https://projects.blender.org/blender/blender/commit/913b71bb8be9  rB913b71bb])
* Add new_meta RNA API function ([https://projects.blender.org/blender/blender/commit/bfad8deb0be0  rBbfad8deb])
* Add move_to_meta RNA API function ([https://projects.blender.org/blender/blender/commit/4158405c0b9d  rB4158405c])
* Disk cache path was changed to include `_seq_cache` suffix ([https://projects.blender.org/blender/blender/commit/5368859a669d 5368859a66])

== Compositor ==

'''Redesigned Cryptomatte Workflow''' [https://projects.blender.org/blender/blender/commit/d49e7b82da67885aac5933e094ee217ff777ac03 d49e7b82da]

In the redesigned workflow a RenderLayer or Image can be selected directly from the
cryptomatte node. The cryptomatte layers are extracted from the meta data.
No need anymore to connect the sockets from the render layer node anymore.

[[File:20210413 cryptomatte-workflow.png|mediumpx|frameless|center|Sampling a matte using the Cryptomatte node]]

When picking a cryptomatte id the mouse shows a hint what is underneath the mouse
cursor. There is no need anymore to switch to the pick socket. You can even pick
from the image editor or movie clip editor.

The matte id's are stored as readable text in the node. It is even possible to alter
the text.

'''Anti Aliasing Node''' [https://projects.blender.org/blender/blender/commit/805d9478109e 805d947810]

The Anti-Aliasing node removes distortion artifacts around edges known as aliasing.
The implementation is based on Enhanced Subpixel Morphological Antialiasing.

[[File:20210413 anti-aliasing-node.png|mediumpx|frame|center|Anti aliasing node]]


== Animation Player ==

* Improved display performance using GLSL to convert the color-space instead of the CPU for float images [https://projects.blender.org/blender/blender/commit/fd3e44492e7606a741a6d2c42b31687598c7d09a fd3e44492e]. 
* Added support for limiting cache using a memory limit instead of a fixed number of frames.<br>This uses the memory cache limit system-preference when launching the player from Blender, otherwise it can be accessed with via a new command line argument `-c <memory_limit_mb>`.  [https://projects.blender.org/blender/blender/commit/3ee49c8711ca72b03687574a98adec904ec1699c 3ee49c8711]
* Image cache is now filled at startup to avoid frame-skipping during playback as frames load [https://projects.blender.org/blender/blender/commit/3ee49c8711ca72b03687574a98adec904ec1699c 3ee49c8711]