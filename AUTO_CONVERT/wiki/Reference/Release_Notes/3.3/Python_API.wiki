= Python API & Text Editor =

== Background Jobs ==

New possibilities to get the status of rendering & baking jobs in Python ([https://projects.blender.org/blender/blender/commit/f4456a4d3c97  rBf4456a4d]):
* New `bpy.app.is_job_running(job_type)` function, which takes `"RENDER"`, `"RENDER_PREVIEW"`, or `"OBJECT_BAKE"` as `job_type` parameter. It returns a boolean to indicate whether a job of that type is currently running.  It is intentional that only a subset of all of Blender's job types are supported here. That way, those "hidden" jobs are kept as they are currently: an internal detail of Blender, rather than a public, reliable part of the API. In the future, the list of supported job types can be expanded on a case-by-case basis, when there is a clear and testable use case.
* New app handlers `object_bake_pre`, object_bake_complete`, object_bake_canceled` that signal respecively that an object baking job has started, completed, or was cancelled.

== Other Additions ==
* [https://docs.blender.org/api/3.3/bpy.props.html#bpy.props.StringProperty `bpy.props.StringProperty`] supports a `search` & `search_options` arguments to support showing candidates when editing string properties. ([https://projects.blender.org/blender/blender/commit/3f3d82cfe9cefe4bfd9da3d283dec4a1923ec22d 3f3d82cfe9])
* XR: Expose OpenXR user paths in XR event data (`XrEventData.user_path/user_path_other`) ([https://projects.blender.org/blender/blender/commit/23be3294ff5 23be3294ff]).
* ID Management: Add utility functions for getting a list of IDs referenced by a given one. ([https://projects.blender.org/blender/blender/commit/26d375467b9 26d375467b]) Here's an example use case:
: <source lang="Python">
from bpy_extras.id_map_utils import get_id_reference_map, get_all_referenced_ids

def suffix_dependents(object: bpy.types.Object, recursive=True, suffix: str):
    """Append suffix the names of datablocks used by an object, including itself."""
    ref_map = get_id_reference_map()
    if recursive:
        # Get all IDs referenced by a Blender Object: Its Mesh, Shape Key, Materials, Textures, Node Graphs, etc.
        datablocks = get_all_referenced_ids(collection, ref_map)
    else:
        # Get only directly referenced IDs: Its Mesh and any IDs referenced via Custom Properties.
        datablocks = ref_map[object]

    datablocks.add(object) # The object itself is not already included.
    for db in datablocks:
        db.name += suffix
</source>
* Add VSE API function to select displayed meta strip. ([https://projects.blender.org/blender/blender/commit/6b35d9e6fbef 6b35d9e6fb])
* Add `mathutils.Matrix.is_identity` read-only attribute to check for an identity matrix. ([https://projects.blender.org/blender/blender/commit/133d398120bfa5c6fe35e93b424cc86543747ccd 133d398120])
* Add `FCurveKeyframePoints.clear()` to delete all keyframe points of an FCurve ([https://projects.blender.org/blender/blender/commit/4812eda3c5d1  rB4812eda3]).
* Add Event `type_prev` & `value_prev` members ([https://projects.blender.org/blender/blender/commit/2580d2bab50f399f101095a197301229614b1d48 2580d2bab5]).

== Other Changes ==

* The `blend_render_info` module now supports reading Z-standard compressed blend files. ([https://projects.blender.org/blender/blender/commit/b3101abcce967c1a623c9e732199b69140a210c0 b3101abcce]).
* The API for adding context menu entries has changed. Instead of manually registering `WM_MT_button_context`, draw functions should be appended to `UI_MT_button_context_menu`, which is registered by Blender itself. The old API is still available for compatibility, but is deprecated and '''will be removed''' in a future release, therefore addons should update to the new API. ([https://projects.blender.org/blender/blender/commit/8a799b00f8fa 8a799b00f8])
: <source lang="Python">
### Old API ###
class WM_MT_button_context(Menu):
    bl_label = "Unused"

	def draw(self, context):
		layout = self.layout
		layout.separator()
		layout.operator("some.operator")

def register():
	bpy.utils.register_class(WM_MT_button_context)

def unregister():
	bpy.utils.unregister_class(WM_MT_button_context)

### New API ###
# Important! `UI_MT_button_context_menu` *must not* be manually registered.
def draw_menu(self, context):
    layout = self.layout
    layout.separator()
    layout.operator("some.operator")

def register():
    bpy.types.UI_MT_button_context_menu.append(draw_menu)

def unregister():
    bpy.types.UI_MT_button_context_menu.remove(draw_menu)
</source>

== Breaking Changes ==
* Strip properties `frame_start`, `frame_offset_start` and `frame_offset_end` are now floating point. ([https://projects.blender.org/blender/blender/commit/302b04a5a3fc 302b04a5a3])
* `SoundSequence.pitch` is replaced with `speed_factor` property. This property is available also for other non-effect strips to control their playback speed. ([https://projects.blender.org/blender/blender/commit/302b04a5a3fc 302b04a5a3])