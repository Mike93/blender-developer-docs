= Modeling = 

== General ==
* New property in the ''Shade Smooth'' operator to enable ''Auto Smooth''. Exposed as '''Shade Auto Smooth''' in the 3D Viewport's ''Object'' menu and ''Object Context Menu''. ([https://projects.blender.org/blender/blender/commit/eef98e66 eef98e66])
* New snapping method that snaps transformed geometry to ''Nearest Face''. Expanded snapping options to give more control over possible snapping targets. ([https://projects.blender.org/blender/blender/commit/011327224ece 011327224e])

== Modifiers ==

* Surface deform modifier can be used when target mesh increases number of vertices. ([https://projects.blender.org/blender/blender/commit/5f8f436dca9 5f8f436dca])

== UV ==

[[File:Uv before.png|thumb|Note the flipped and overlapping UVs around the nose, and the shear and non-uniform scale for the ears and eyes.]]
[[File:Uv after.png|thumb|After minimize stretch, the flipped faces are smoothed away. After Average Island Scale with Shear and Scale UV, the ears and eyes now have a nice even balance between the U coordinate and V coordinate.]]

Four changes help with workflow and improve final UV quality.
* When using UV Unwrap in the UV editor, only the selected UVs will be unwrapped. ([https://projects.blender.org/blender/blender/commit/c0e453233132 c0e4532331])
* Minimize Stretch will now unflip faces. ([https://projects.blender.org/blender/blender/commit/135e530356d0 135e530356])
* Upgrade Average Island Scale with new options, Scale UV and Shear. ([https://projects.blender.org/blender/blender/commit/931779197a9c 931779197a])
* Add new operator, "UV Select Similar". ([https://projects.blender.org/blender/blender/commit/1154b4552679 1154b45526])


In addition, many smaller bugs have been fixed or resolved including:
* Snapping to UV vertices is now done if the cursor reaches a minimum distance. ([https://projects.blender.org/blender/blender/commit/d2271cf93926 d2271cf939])
* Improved constrain-to-image-bounds when scaling UVs. ([https://projects.blender.org/blender/blender/commit/bc256a450770 bc256a4507], [https://projects.blender.org/blender/blender/commit/4d7c99018008 4d7c990180])
* Use per-face aspect correction when applying Cube Projection, Spherical Projection, Cylinder Projection and Project From View. ([https://projects.blender.org/blender/blender/commit/1c1e8428791f 1c1e842879])
* Update size of hitbox for UV axis gizmo so it matches hitbox size in the 3D viewport. ([https://projects.blender.org/blender/blender/commit/65e7d4993933 65e7d49939])
* During UV unwrap, pinned vertices will not be merged. ([https://projects.blender.org/blender/blender/commit/e6e9f1ac5a2d e6e9f1ac5a])
* Improved UDIM support.