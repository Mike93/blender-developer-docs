= Python API & Text Editor =

== Custom Script Directories ==

[[File:Release notes multiple script dirs prefs.png|thumb]]
Multiple script directory paths can now be configured in the Preferences (''File Paths'' section). Each of these directories supports the regular script directory layout with a startup file (or files?), add-ons, modules and presets. ([https://projects.blender.org/blender/blender/commit/ba25023d22 ba25023d22])

Compatibility notes:
* `bpy.types.PreferencesFilePaths.script_directory` is deprecated. The directories are exposed in `bpy.types.script_directories` now.
* `bpy.utils.script_path_pref` is removed, use `bpy.utils.script_paths_pref` instead.
<br style="clear:both;">

== Blender Handlers ==

* Added `bpy.app.handlers` (`save_post_fail`, `load_post_fail`), so the `*_pre` handles will always call a `*_post` handler, even on failure ([https://projects.blender.org/blender/blender/commit/46be42f6b16314f59a37ebb430d77d12e7a88461 46be42f6b1]).
* File load handlers (`save_{pre/post/post_fail}`, `load_{pre/post/post_fail}`) now accept a filepath argument so the file being loaded or saved is known ([https://projects.blender.org/blender/blender/commit/46be42f6b16314f59a37ebb430d77d12e7a88461 46be42f6b1])

== Internal Mesh Format ==
The mesh data structure refactoring from earlier releases has continued in 3.6. See the similar sections in the [[Reference/Release_Notes/3.4/Python_API#Internal_Mesh_Format|3.4]] and [[Reference/Release_Notes/3.5/Python_API#Internal_Mesh_Format|3.5]] release notes.
* The vertex and edge indices stored for mesh face corners (`MeshLoop`/`MLoop`) are now stored as separate attributes, named `.corner_vert` and `.corner_edge` ([https://projects.blender.org/blender/blender/commit/16fbadde363c8074ec725fa1e1079add096bd741 16fbadde36]).
* Mesh faces (polygons) are now stored with a single integer internally, rather than the `MPoly` type.
** This means that the order of faces is always consistent with the order of face corners (loops).
** The `MeshPolygon.loop_total` property is no longer editable. Instead the size of each face should be changed with the next face's `loop_start` property.
* Mesh edges are now stored in a generic attribute named `.edge_verts` ([https://projects.blender.org/blender/blender/commit/2a4323c2f51f92fc1c88561cbf23004b42138ad2 2a4323c2f5]).
** A new 2D integer vector attribute type is added to store edge data ([https://projects.blender.org/blender/blender/commit/988f23cec3912dac96595c652a5f4e427d7550c8 988f23cec3]).
* UV seams are now stored as a generic attribute, accessible with the `.uv_seam` name on the edge domain ([https://projects.blender.org/blender/blender/commit/cccf91ff832d119dbf048b0518a696b9aa83bce4 cccf91ff83]).
* The smooth/sharp status for faces is now stored as a generic attribute with the `sharp_face` name ([https://projects.blender.org/blender/blender/commit/5876573e14f434a4cd8ae79c69afbe383111ced9 5876573e14]).
** In some cases, meshes are now '''smooth by default''' (when created from scratch, without `from_pydata`).
*** To simplify getting the previous behavior, new API functions `Mesh.shade_flat()` and `Mesh.shade_smooth()` have been added ([https://projects.blender.org/blender/blender/commit/ee352c968fd165fafd17adcc696e98bb0efa844a ee352c968f]).

== Other Changes ==

* New `bpy_extras.node_utils.connect_sockets()` function to allow creating links between virtual sockets (grayed out sockets in Group Input and Group Output nodes) ([https://projects.blender.org/blender/blender/commit/81815681d0aeaae719c6cb736f0326201c87ba4a 81815681d0]).
* New `action_tweak_storage` property in `AnimData`, which exposes the temporary storage used to stash the main action when tweaking an NLA clip ([https://projects.blender.org/blender/blender/commit/997ad50b4996fb410733acb53eb5c5e6caedbe61 997ad50b49]).