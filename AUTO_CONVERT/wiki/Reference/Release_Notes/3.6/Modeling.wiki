= Modeling =

== UV Editing ==

=== UV Packing ===

[[File:UVPack36.png|thumb|New '''Concave Packing''' shape method]]
The UV Packing engine was upgraded, dramatically improving performance on large meshes and improving support for non-square materials.

On many real world cases, efficiency of the layout has also been increased.

In addition, many new features were implemented:

* Added an option to choose the shape of the UV Packing approximation ("Bounding Box", "Exact Shape" and "Convex Hull"), giving layouts which use much more of the available space ([https://projects.blender.org/blender/blender/commit/e0d05da8262d84d21a0a1bfb0c7a48d9c29a216c e0d05da826]).

* Added new UV Packing option "Merge Overlapped", where overlapping islands stick together during UV packing ([https://projects.blender.org/blender/blender/commit/b601ae87d064cbde29e8ae7191ebba54213c709a b601ae87d0]).

* Added new UV Packing option "Pack To > Original Bounding Box", where islands are packed back into the original bounding box of the selection. ([https://projects.blender.org/blender/blender/commit/957ac41237fb19e03956dda0a293455b604a95ac 957ac41237]).

<br style="clear:both;">

=== Improvements ===
Further UV changes include:

[[File:UVOrange.png|thumb|Unwrapping like an orange-peel. Careful seam placement allows an extended unwrap using the new manual seam placement feature in '''UV Sphere Projection'''.]]
* '''UV Sphere Projection''' and '''UV Cylinder Projection''' now support manual placement of seams ([https://projects.blender.org/blender/blender/commit/6b8cdd5979964f39b01bc4def194255fa3766a40 6b8cdd5979]).

* The '''UV Select Similar''' operator has new options for '''Similar Winding''' and for '''Similar Object'''. ([https://projects.blender.org/blender/blender/commit/2b4bafeac68e 2b4bafeac6]).

<br style="clear:both;">

== Performance ==

* Conversion from edit meshes to object mode meshes has been parallelized, leading to better performance when exiting edit mode ([https://projects.blender.org/blender/blender/commit/5669c5a61b2f049ec1b55cf045221b24095b9df2 5669c5a61b]).
** The conversion's performance was also improved by around 75% when there are multiple UV maps ([https://projects.blender.org/blender/blender/commit/0fe0db63d7f 0fe0db63d7]).
* Face corner "split" normal calculation performance is improved by up to 80%, with improvements in memory usage as well ([https://projects.blender.org/blender/blender/commit/9fcfba4aae7 9fcfba4aae], [https://projects.blender.org/blender/blender/commit/3c436326517 3c43632651], [https://projects.blender.org/blender/blender/commit/9292e094e77 9292e094e7]).
** With custom split normals data, performance has been improved by up to 44% in addition ([https://projects.blender.org/blender/blender/commit/17d161f56537d424d369a1dcf4c47d3976842af8 17d161f565]).
* Subdivision surface performance is slightly improved on large meshes with no loose vertices ([https://projects.blender.org/blender/blender/commit/3507431c306 3507431c30]).
* Extracting UV map data for viewport drawing can be up to 3x faster ([https://projects.blender.org/blender/blender/commit/63a44e29aca 63a44e29ac]).

== Compatibility ==

* When baking normals with custom normals data, behavior may be different if the auto smooth angle is not 180 degrees ([https://projects.blender.org/blender/blender/issues/107930 #107930]).