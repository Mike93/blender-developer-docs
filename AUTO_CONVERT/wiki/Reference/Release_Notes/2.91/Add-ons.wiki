= Blender 2.91: Add-ons =
= Add-ons Updates =
== BlenderKit ==
[[File:Hair material preview - blenderkit.png|thumb|right|]]
* 2 new material previews were introduced:
** Complex Ball. This preview is intended to show off specific material properties - edge wear or cavity detail, translucency and SSS, refraction.
** Hair preview.
* Selected model panel was improved, now all commands work as expected.







== Collection Manager ==

{| class="transparent"
 |valign=top|[[File:Collection Manager 2.91 Release Notes.png|429px|thumb|none|Collection Manager popup with new Holdout and Indirect Only RTOs]]
 |valign=top|[[File:QCD Screenshot 2.91.png|429px|thumb|none|QCD widgets with the new Quick View Toggles button to the left of the header widget.
]]
 |}

=== New Features ===

==== CM Popup ====
* Added support for the Holdout and Indirect Only RTOs. ([https://projects.blender.org/blender/blender-addons/commit/559fbf90  rBA559fbf9])
* Include new collections in the current filter until the filtering changes. ([https://projects.blender.org/blender/blender-addons/commit/52fb8e51  rBA52fb8e5])
* Allow all filters to be combined with each other. ([https://projects.blender.org/blender/blender-addons/commit/b66e1363  rBAb66e136])


==== QCD ====
[[File:Quick View Toggles Blender 2.91.png|right|thumb|Quick View Toggles]]
* Added the ability to select all objects in a QCD slot when alt-clicking on the slot. ([https://projects.blender.org/blender/blender-addons/commit/1d1bb1a7  rBA1d1bb1a])
* Added Quick View Toggles for influencing QCD setup/visibility. ([https://projects.blender.org/blender/blender-addons/commit/bf176041  rBAbf17604])
** Enable All QCD Slots.
** Enable All QCD Slots Isolated.
** Disable All Non QCD Slots.
** Disable All Collections.
** Select All QCD Objects.
** Discard QCD History.


=== Bug Fixes ===

==== CM Popup ====
* Fixed the active object sometimes getting lost when performing actions with the exclude checkbox. ([https://projects.blender.org/blender/blender-addons/commit/975f81d2  rBA975f81d])
* Prevent new collections from being added when the selected collection isn't visible. ([https://projects.blender.org/blender/blender-addons/commit/52fb8e51  rBA52fb8e5])


==== QCD ====
* Fixed bugs with QCD slot switching. ([https://projects.blender.org/blender/blender-addons/commit/bf176041  rBAbf17604])
* Fixed the active object sometimes getting lost when toggling QCD slots. ([https://projects.blender.org/blender/blender-addons/commit/bf176041  rBAbf17604])
* Fixed the layout and display of theme overrides for the OpenGL move widget in the preferences. ([https://projects.blender.org/blender/blender-addons/commit/b8159369  rBAb815936])