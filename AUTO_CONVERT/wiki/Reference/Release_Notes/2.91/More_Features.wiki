= Blender 2.91: More Features =

== UV/Image Editor ==

* Alpha blending are now done in Linear Scene Reference Space.
* Support of pure emissive colors.
* Improved performance; especially noticeable when viewing render results and repeating image.
* The smoothness of the UV's can now be controlled in the User preferences ([https://projects.blender.org/blender/blender/commit/b2fc0678542a b2fc067854])

== Sequencer ==

The FFmpeg library is now compiled with vectorization (SSE/AVX/AVX2) support enabled, allowing for faster video decoding and encoding. In the tests re-encoding 3405 2560x1376 frames on Xeon E5-2699 V4 CPU overall time down from '''313''' seconds to '''210''' seconds ([https://storage.googleapis.com/institute-storage/vse_simplified_example.zip sample file]).

== Motion tracking ==

* Implemented Brown-Conrady distortion model, improving compatibility with other software ([https://projects.blender.org/blender/blender/commit/3a7d62cd1f5 3a7d62cd1f])

== Shaders ==

* Add Emission Strength to Principled BSDF shader, as a multiplier to the Emission color. ([https://projects.blender.org/blender/blender/commit/b248ec9 b248ec9])

== Cycles ==

* Motion blur rendering from Alembic files. ([https://projects.blender.org/blender/blender/commit/b5dcf746369e b5dcf74636])
* Overriding the compute device from the command line. ([https://projects.blender.org/blender/blender/commit/cfa101c22871 cfa101c228])
* NVIDIA RTX 30xx graphics cards support without runtime compilation.
* For AMD graphics cards, the latest Pro drivers must be installed. These contain critical fixes for Cycles OpenCL rendering to work.