= Core =

=== Wayland on Linux ===

* Input Method Editors (IME) now supported ([https://projects.blender.org/blender/blender/commit/a38a49b073f582a0f6ddcca392f2760afdc4d5ed a38a49b073]).