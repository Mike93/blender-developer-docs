= Pipeline, Assets, IO =

== Asset System & Asset Browser ==

* Collection assets are now supported ([https://projects.blender.org/blender/blender/commit/253e4e7ed22b  rB253e4e7e])
** An automatic preview image is generated when using ''Mark as Asset'' ([https://projects.blender.org/blender/blender/commit/810e225c260d  rB810e225c]).
** Collection instancing can be toggled in the ''Adjust Last Operation'' panel after dropping to the 3D View ([https://projects.blender.org/blender/blender/commit/eb1ede569316  rBeb1ede56]).
* Ability to drag materials (from Asset Browser for example) to Material slots in Properties editor. ([https://projects.blender.org/blender/blender/commit/fd2519e0b694 fd2519e0b6])

== Wavefront Obj I/O ==

New '''experimental OBJ importer'''. ([https://projects.blender.org/blender/blender/commit/e6a9b223844  rBe6a9b223], [https://projects.blender.org/blender/blender/commit/213cd39b6db3  rB213cd39b])

The new importer is written in C++ and is much faster than the Python importer, while using less memory as well. Import time comparisons on several large scenes:

* rungholt.obj Minecraft level (269MB file, 1 object): 54.2s -> 5.9s, memory usage 7.0GB -> 1.9GB during import.
* Blender 3.0 splash scene (2.4GB file, 24000 objects): 4 hours -> 53s.


'''Experimental OBJ exporter''': Speed improvements. ([https://projects.blender.org/blender/blender/commit/1f7013fb90b3  rB1f7013fb], [https://projects.blender.org/blender/blender/commit/e2e4c1daaa  rBe2e4c1da]).

Export time comparisons between Blender 3.1.1 and 3.2:

* Suzanne, subdivided to 6th level (330MB obj file):
** Windows (32 threads): 6.0s -> 1.0s
** macOS (10 threads): 3.9s -> 1.2s
** Linux (48 threads): 6.2s -> 1.4s
* Blender 3.0 splash scene (2.4GB obj file):
** Windows (32 threads): 45.5s -> 3.9s
** macOS (10 threads): 27.2s -> 5.5s
** Linux (48 threads): 33.4s -> 4.4s

== Media Formats ==

Blender now has support for the WebP image format which works similar to PNG but compresses faster and generates smaller file sizes. ([https://projects.blender.org/blender/blender/commit/4fd0a69d7ba  rB4fd0a69d])