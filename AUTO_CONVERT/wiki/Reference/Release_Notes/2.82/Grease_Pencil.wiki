= Blender 2.82: Grease Pencil =

== User Interface ==
* Added missing Fill tool properties to property editor panel ([https://projects.blender.org/blender/blender/commit/7d3ea8f12ba8 7d3ea8f12b])
* Updated Dope Sheet channels/layers ([https://projects.blender.org/blender/blender/commit/98ff6cfa575bbe9680e5a0abf176a9d748ecc2b8  rB98ff6cfa])
** Added the Add New Layer, Remove Layer and Move Up/Down buttons to the header
** Added Opacity, Blend and Onion skinning controls to the Channels
** Added extra layers properties in the Side panel

[[File:Dope Sheet.png|800px|thumb|center|New Dope Sheet channels controls and Side Panel]]

* Simplify panel options update ([https://projects.blender.org/blender/blender/commit/96a1bc2997c871a3405dd0a41d780eee49c2c8bb  rB96a1bc29])

== Operators ==
== Tools ==
* New eyedropper tool for creating new materials. ([https://projects.blender.org/blender/blender/commit/5adb3b6882f4 5adb3b6882])
* New Polyline primitive. ([https://projects.blender.org/blender/blender/commit/c2a2cd13be2f c2a2cd13be])

== Modifiers ==

* New Multiple Strokes modifier to generate multiple strokes around the original ones. ([https://projects.blender.org/blender/blender/commit/91248876e517  rB91248876])

[[File:Multiple Stroke Modifier.png|800px|thumb|center|Multiple Strokes Modifier]]