= Geometry Nodes =

== Viewer Node ==

* The ''Viewer Node'' now has the ability to preview geometry and attributes in the viewport as well as the spreadsheet ([https://projects.blender.org/blender/blender/commit/c55d38f00b8c  rBc55d38f0]).
** When a field is linked to the second input of the viewer node it is displayed as an overlay in the viewport.
** A viewer node is activated by clicking on it.
** The attribute overlay opacity can be controlled with the "Viewer Node" setting in the overlays popover.
** A viewport can be configured not to show intermediate viewer-geometry by disabling the "Viewer Node" option in the "View" menu. 
** Viewer nodes now have a domain dropdown. It determines which domain the field will be evaluated on.
[[File:2022-09-28 13-06-14.mp4|center|thumb|800px]]

=== General ===
* The '''Self Object''' retrieves the current modifier object for retrieving transforms ([https://projects.blender.org/blender/blender/commit/dd5131bd700c7e dd5131bd70]).
* The ''Transfer Attribute'' node has been removed and split into multiple more specific nodes ([https://projects.blender.org/blender/blender/commit/dedc679ecabb43 dedc679eca]).
** The '''Sample Index''' node retrieves data from specific geometry elements by index.
** The '''Sample Nearest''' node retrieves the indices from the closest geometry elements
** The '''Sample Nearest Surface''' node interpolates a field input to the closest location on a mesh surface.

=== Meshes ===
* The new '''Face Set Boundaries''' node finds the edges between different patches of faces ([https://projects.blender.org/blender/blender/commit/3ff15a9e23bd8a 3ff15a9e23]).
* Access to mesh topology information has been improved, with new nodes and other changes .
** '''Corners of Face''' Retrieves corners that make up a face ([https://projects.blender.org/blender/blender/commit/482d431bb673 482d431bb6]).
** '''Corners of Vertex''' Retrieves face corners connected to vertices ([https://projects.blender.org/blender/blender/commit/482d431bb673 482d431bb6]).
** '''Edges of Corner''' Retrieves the edges on boths sides of a face corner ([https://projects.blender.org/blender/blender/commit/482d431bb673 482d431bb6]).
** '''Edges of Vertex''' Retrieves the edges connected to each vertex ([https://projects.blender.org/blender/blender/commit/482d431bb673 482d431bb6]).
** '''Face of Corner''' Retrieves the face each face corner is part of ([https://projects.blender.org/blender/blender/commit/482d431bb673 482d431bb6]).
** '''Offset Corner in Face''' Retrieves neighboring corners within a face ([https://projects.blender.org/blender/blender/commit/482d431bb673 482d431bb6]).
** '''Vertex of Corner''' Retrieves the vertex each face corner is attached to ([https://projects.blender.org/blender/blender/commit/482d431bb673 482d431bb6]).
** The ''Edge Vertices'' node now has an index input so it can be more easily combined with the other nodes ([https://projects.blender.org/blender/blender/commit/4ddc5a936e07 4ddc5a936e]).
* The new '''Sample UV Surface''' node allows getting an attribute value based on a UV coordinate ([https://projects.blender.org/blender/blender/commit/e65598b4fa2059 e65598b4fa]).

=== Curves ===
* The ''Trim Curves'' node now supports cyclic curves ([https://projects.blender.org/blender/blender/commit/eaf416693dcb43 eaf416693d]).
* New nodes give access to topology information about the mapping between curves and points.
** '''Curve of Point''' Retrieves the curve a control point is part of ([https://projects.blender.org/blender/blender/commit/482d431bb673 482d431bb6]).
** '''Points of Curve''' Retrieves a point index within a curve ([https://projects.blender.org/blender/blender/commit/482d431bb673 482d431bb6]).
** '''Offset Point in Curve''' Offset a control point index within its curve ([https://projects.blender.org/blender/blender/commit/8a6dc0fac71c 8a6dc0fac7], ([https://projects.blender.org/blender/blender/commit/248def7e4806 248def7e48])).
* The '''Set Curve Normal''' allows choosing the normal evaluation mode for curves ([https://projects.blender.org/blender/blender/commit/748fda32ede8 748fda32ed]).
* The '''Sample Curve''' node now has inputs for the curve index and a custom value to sample ([https://projects.blender.org/blender/blender/commit/5f7ca5462d31  rB5f7ca546]).

=== Volumes ===
* The '''Distribute Points in Volume''' node creates points inside of volume grids ([https://projects.blender.org/blender/blender/commit/b6e26a410cd29f b6e26a410c]).

=== Instances ===
* Attributes of instances created via geometry nodes are now accessible from materials through the Instancer mode of the Attribute node ([https://projects.blender.org/blender/blender/commit/2f7234d3e1bbc 2f7234d3e1]).

=== Performance ===
* Geometry nodes has a new evaluation system ([https://projects.blender.org/blender/blender/commit/4130f1e674f83f 4130f1e674]).
** The evaluator can provide better performance when many complex node groups are used.
** Performance can be increased when there are very many small nodes ([https://projects.blender.org/blender/blender/commit/5c81d3bd469121 5c81d3bd46]).
* The ''Trim Curves'' node has been ported to the new data-block and can be 3-4 times faster than in 3.2 or 3.3 ([https://projects.blender.org/blender/blender/commit/eaf416693dcb43 eaf416693d]).
* Mesh and curve domain interpolation can be skipped for single values ([https://projects.blender.org/blender/blender/commit/e89b2b122135 e89b2b1221], [https://projects.blender.org/blender/blender/commit/535f50e5a6a2 535f50e5a6]).

== Node Editor ==

=== Assets ===
* Node group assets are visible in the add menus of the node editor ([https://projects.blender.org/blender/blender/commit/bdb57541475f20 bdb5754147]).

=== User Interface ===
* Show data-block use count in the Context Path and Group node header ([https://projects.blender.org/blender/blender/commit/84825e4ed2e0 84825e4ed2]).

[[File:Geometry Nodes Use Count.png]]

* Node links have been tweaked to make them fit better with the overall look of the node editor ([https://projects.blender.org/blender/blender/commit/9a86255da8dcf0 9a86255da8]).
** Link curving has been adjusted to make vertical links look better ([https://projects.blender.org/blender/blender/commit/67308d73a4f9ec 67308d73a4]).
* Input sockets are reused when creating new node groups ([https://projects.blender.org/blender/blender/commit/14e4c96b64f9  rB14e4c96b]).

=== Duplicate Linked ===
* Duplicate Linked operator and user Preference option for Node Tree data duplication ([https://projects.blender.org/blender/blender/commit/6beeba1ef5ae  rB6beeba1e]).

[[File:Nodetree duplication.png|center|frame]]