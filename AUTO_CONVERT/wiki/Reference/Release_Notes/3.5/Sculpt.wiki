= Sculpt, Paint, Texture =

== New Features ==
[[File:Sculpt-paint sculpt vdm example.png|center]]
* Support for '''Vector Displacement Map''' (VDM) brushes for the Draw brush ([https://projects.blender.org/blender/blender/commit/39f63c8c086dd9dbe286924909e400d0d47b186c 39f63c8c08])

* Added '''Extrude Mode''' for Trim tools. See manual for [https://docs.blender.org/manual/en/3.5/sculpt_paint/sculpting/tools/box_trim.html Box Trim], [https://docs.blender.org/manual/en/3.5/sculpt_paint/sculpting/tools/lasso_trim.html Lasso Trim]. ([https://projects.blender.org/blender/blender/commit/88e9826529d1  rB88e98265])

== User Manual ==
Visit the user manual at [https://docs.blender.org/manual/en/3.5/sculpt_paint/sculpting/introduction/general.html blender.org/manual]

* The user manual got a major rewrite of many pages. ([https://ro.developer.blender.org/rBM9824 rBM9824])
* Individual tool pages were rewritten and restructured. ([https://ro.developer.blender.org/rBM9839 rBM9839])
* Additions and rewrites for Editing pages ([https://ro.developer.blender.org/rBM9885 rBM9885])

This adds:
* A multi-page Introduction section to sculpting
* User oriented sorting and more visual examples
* Fixing various out of date, missing or false information
* A new page for Expand to explain many use cases

[[File:Sculpt user manual intro.png|600px|thumb|center]]

== Shortcut Changes ==

* The shortcuts `Shift R` and `Shift D` to define density in sculpt mode have been remapped to `R` ([https://projects.blender.org/blender/blender/commit/3e9039091870  rB3e903909])

This change was done to prevent shortcut conflicts with Redo. Based on community feedback, not only this improves consistency across Blender, but the benefits of being able to redo certain operations outweigh the muscle memory adjustment.

This change affects the Voxel Remesher, Dynamic Topology and the Hair Density brush.