= Animation & Rigging =

== General ==

* The old (pre-3.0) pose library has been removed ([https://projects.blender.org/blender/blender/commit/48b5dcdbe857  rB48b5dcdb]). They are now seen by Blender as simply Actions with named markers. It is recommended to [https://docs.blender.org/manual/en/3.5/animation/armatures/posing/editing/pose_library.html#converting-old-pose-libraries convert them to pose assets].
* Fixed an old bug where the effect of the Invert Vertex Group toggle of the Armature modifier was inverted when Multi-Modifier was active. Old tutorials explaining the usage of the Multi-Modifier option will need updating. ([https://projects.blender.org/blender/blender/commit/ea1c31a24438  rBea1c31a2])
* Added the pin icon to the Dope Sheet to pin channels. ([https://projects.blender.org/blender/blender/commit/49ad91b5ab7b  rB49ad91b5])
* Added "Select Linked Vertices" to weight paint mode. ([https://projects.blender.org/blender/blender/commit/04aab7d51620  rB04aab7d5])
* Take subframes into account when jumping to next/previous keyframes. ([https://projects.blender.org/blender/blender/commit/5eab813fc078  rB5eab813f])
* Motion paths have a new frame range option "Manual Range" to ensure the frame range is never changed on update ([https://projects.blender.org/blender/blender/commit/d72c7eefd1c5  rBd72c7eef]). In other modes ("All Keys", "Selected Keys", "Scene Frame Range") the start/end frame are still editable, but greyed out.
* Adding F-Curve modifiers to multiple channels at once is now easier to access ([https://projects.blender.org/blender/blender/commit/0f51b5b599bb  rB0f51b5b5]). 

[[File:Blender-3.5-anim-motion-paths.webp|600px|Motion Paths frame range options]]

== Asset Browser and Pose Library ==

The Pose Library went through a few usability changes to unify the experience of the other asset types ([https://projects.blender.org/blender/blender-addons/commit/c164c5d86655  rBAc164c5d]).

[[File:Poselib assetbrowser.png|800px]]

The Pose Library functionality in the asset browser were moved to a new Asset menu, as well as the pose asset context menu.

[[File:Poselib contextmenu.png]]

In the viewport, a few options were removed:
* The option to Create Pose Assets is no longer there - use instead the Action Editor or the Asset Browser.
* The Flip Pose check-box is gone - flipped poses can be applied directly via the context menu. When blending, keep CTRL pressed to blend the flipped pose.
* The `poselib.apply_pose_asset_for_keymap` and `poselib.blend_pose_asset_for_keymap` operators are gone. If you have assigned these in your keymap, use the regular apply/blend operators (`poselib.blend_pose_asset` and `poselib.apply_pose_asset`) instead.

Other improvements are:
* A pose asset can now be "subtracted" while blending. Drag to the right to blend as usual, '''drag to the left to subtract the pose''' ([http://projects.blender.org/scm/viewvc.php?view=rev&root=bf-blender&revision=c82311bb8efd24abec6f50a18256c636b78ef626 rc82311bb8efd24abec6f50a18256c636b78ef626]).
* While blending, keep Ctrl pressed to flip the pose ([https://projects.blender.org/blender/blender/commit/bd36c712b928f522  rBbd36c712]).
* Blending can now also exaggerate a pose, by pressing E (for Extrapolate) and applying a pose for more than 100% [https://projects.blender.org/blender/blender/commit/74c4977aeaa3  rB74c4977a].

[[File:Blender-3.5-poselib-bidirectional-blending.mp4]]

== Graph Editor ==

==== Ease Operator ====
The Graph Editor got a new Operator, "Ease" ([https://projects.blender.org/blender/blender/commit/76a68649c1c1 76a68649c1]), that can align keys on an exponential curve.
This is useful for quickly making an easing transition on multiple keys.
It can be found under <span class="literal">Key</span>&nbsp;» <span class="literal">Slider Operators</span>.

[[File:Graph Editor Ease Operator.mp4|thumb|1280px|center]]


== Pose Mode ==

==== Propagate Pose ====
The Propagate Pose Operator has been updated ([https://projects.blender.org/blender/blender/commit/200a114e1596 200a114e15]). Previously it used to evaluate each FCurve individually to find the target key.
That resulted in undesired behavior when the keys where not on the same frame.
Now it finds the frame first by checking all FCurves in question and then propagates the pose to it. It will add a keyframe to it in case it doesn't exist.
The discussion can be found here https://developer.blender.org/T87548

[[File:Pose Propagate Demo.mp4|thumb|1280px|center]]