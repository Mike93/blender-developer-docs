= Grease Pencil =
== Operators ==
* Sculpt Auto-masking moved to Global setting. ([https://projects.blender.org/blender/blender/commit/4c182aef7ce0 4c182aef7c])

[[File:Auto-masking.png|600px]]

* Added new Auto-masking pie menu using `Shift+Alt+A`. ([https://projects.blender.org/blender/blender/commit/4c182aef7ce0 4c182aef7c])

[[File:Auto-masking radial.png|600px]]

* Interpolate Sequence by default can now use all different keyframes types as extremes and a new option `Exclude Breakdowns` was added to allows users exclude this type of keyframes from the interpolation as with the old behavior. ([https://projects.blender.org/blender/blender/commit/56ae4089eb35 56ae4089eb])

[[File:Interpolation.png|600px]]

* Copy & Paste now works in multiframe mode. ([https://projects.blender.org/blender/blender/commit/a5d4c63e867e a5d4c63e86])

== UI ==
* `Vertex Opacity` parameter now is visible in Overlay panel in Sculpt mode. ([https://projects.blender.org/blender/blender/commit/1aff91b1a707 1aff91b1a7])

[[File:Vertex Opacity Sculpt Mode.png|600px]]

* New option to show the brush size in Draw tool cursor. ([https://projects.blender.org/blender/blender/commit/a44c1284823a a44c128482]) ([https://projects.blender.org/blender/blender/commit/a2cf9a8647fd a2cf9a8647])

[[File:Cursor.png|600px]]

* Radial control now displays the correct size when resizing brush size. ([https://projects.blender.org/blender/blender/commit/0fb12a9c2ebc 0fb12a9c2e])
* Create new layer using `Y` menu in Drawing mode allows to enter new layer name. ([https://projects.blender.org/blender/blender/commit/f53bb93af937 f53bb93af9])

[[File:New Layer.png|600px]]

* Material popover now display Fill color for Fill materials. ([https://projects.blender.org/blender/blender/commit/e144af1f7cd3 e144af1f7c])

== Modifiers ==
* Add offset (Location, Rotation, Scale) by Layer, Stroke and Material to the `Offset Modifier`. ([https://projects.blender.org/blender/blender/commit/7d712dcd0bbd 7d712dcd0b])

[[File:GP Offset Modifier.png|600px]]

* New `Natural Drawing Speed` mode in `Build Modifier` timing property that replay drawings using the recorded speed of the stylus, giving it a more natural feel ([https://projects.blender.org/blender/blender/commit/250eda36b8f9 250eda36b8])

[[File:GP Build Modifier.png|600px]]