= Core =

=== Custom Properties ===

Custom properties can now have a boolean type ([https://projects.blender.org/blender/blender/commit/ef68a37e5d55  rBef68a37e]).

=== Datablocks ===

'Fake User' on linked data will now always enforce that linked data-block to be considered as 'directly linked', i.e. never automatically cleared by Blender on file save. See [https://projects.blender.org/blender/blender/issues/105786 #105786] and [https://projects.blender.org/blender/blender/commit/133dde41bb 133dde41bb] for details.