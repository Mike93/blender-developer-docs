= Building Blender on Linux =

== Distributions ==

Instructions to build Blender, based on your distro of choice.

* [[Building_Blender/Linux/Arch|Arch]]
* [[Building_Blender/Linux/Fedora|Fedora]] 
* [[Building_Blender/Linux/Gentoo|Gentoo]]
* [[Building_Blender/Linux/OpenSUSE|OpenSUSE]]
* [[Building_Blender/Linux/Ubuntu|Ubuntu]]

----

* [[Building_Blender/Linux/Generic_Distro|Generic Distro]]

== Release Environment ==

Setup of official Linux build environment for releases.

* [[Building_Blender/Other/Rocky8ReleaseEnvironment|Rocky 8]] for Blender 3.5 and newer.
* [[Building_Blender/Other/CentOS7ReleaseEnvironment|CentOS 7]] for Blender 3.4 and older.