= Build Options =

By default Blender builds will be similar to official releases. Many build options are available for debugging, faster builds, and to enable or disable various features.

== Build Targets ==

There are a few build targets to choose the overall type of build.

* <tt><b>make</b></tt> - build with all features enabled, except Cycles GPU binaries
* <tt><b>make lite</b></tt> - build with the absolute minimum amount of features enabled, either for faster builds or to avoid installing many dependencies
* <tt><b>make release</b></tt> - complete build with all options enabled including [[Building_Blender/GPU Binaries|Cycles GPU binaries]], matching the releases on blender.org
* <tt><b>make headless</b></tt> - build designed to run from the command line without an interface (for renderfarm or server automation)
* <tt><b>make bpy</b></tt> - build [[Building_Blender/Other/BlenderAsPyModule|as a python module]] which can be loaded from python directly

For a full list of targets, run <tt><b>make help</b></tt>.
== Setup For Developers ==

=== Debug Builds ===

On Windows in Visual Studio or Xcode on macOS, a single build folder can contain both release and debug builds, and you can switch between them in the IDE.

For other platforms, the easiest way to set up a debug build is to build the debug target. This will create a separate build in <tt>../build_<platform>_debug</tt>.

<source lang="text">
make debug
</source>

The build type of an existing build can also be changed by setting <tt>CMAKE_BUILD_TYPE</tt> in the CMake configuration to either <tt>Debug</tt> or <tt>RelWithDebInfo</tt>.

=== Developer Options ===

We recommend developers to configure CMake to enable [[Tools/Debugging/GCC_Address_Sanitizer|address sanitizer]], automated tests and options for faster builds. More details about [[Tools|tools for Blender development are here]].

The most common options can be enabled by using the <tt>developer</tt> target, which can be combined with other targets. For example:

<source lang="text">
make developer
</source>

Or to combine it with a debug build as you usually would:

<source lang="text">
make debug developer
</source>

=== Ninja ===

For faster builds, the [https://ninja-build.org/ Ninja build system] can be used. If ninja is installed and available in the path, it can be added to the <tt>make</tt> command when setting up the build folder. If there already exists a build folder with a different build system, the folder must be removed first. Example command to set up ninja build:

<source lang="text">
make debug developer ninja
</source>

=== Caching ===

Caching helps make rebuilds faster, especially when switching between git revisions and branches.

==== Linux and macOS ====

If [https://ccache.dev/ ccache] is installed, it can be used as follows:
<pre>
make ccache
</pre>

It can be combined with other options, for example:
<pre>
make debug developer ccache ninja
</pre>

The equivalent of the above for Xcode generator would be:
<pre>
cmake -S . -B ../build_xcode -C build_files/cmake/config/blender_developer.cmake -DWITH_COMPILER_CCACHE=ON -G Xcode
</pre>

==== Windows ====

If [https://github.com/mozilla/sccache sccache] is installed and available in the path it can be used, however it will only function with the ninja generator. 

<source lang="text">
make developer ninja sccache 
</source>

== Editing CMake Options ==

By default, the <tt>CMakeCache.txt</tt> configuration file will be in <tt>../build_&lt;platform&gt;</tt>. There are a multiple ways to edit it.

* Editing <tt>CMakeCache.txt</tt> in a text editor.
* Opening <tt>CMakeCache.txt</tt> with the CMake GUI, to easily change options and re-configure. For example on Linux:
:<source lang="bash">cmake-gui ../blender</source>
* Using '''ccmake''', for a command line text interface to easily change options and re-configure.
:<source lang="bash">ccmake ../blender</source>
* cmake parameters can also be set on the command line, for example:
:<source lang="bash">
cmake ../blender \
    -DCMAKE_INSTALL_PREFIX=/opt/blender \
    -DWITH_INSTALL_PORTABLE=OFF \
    -DWITH_BUILDINFO=OFF
</source>
: These commands are exactly those found in <tt>CMakeCache.txt</tt> so you can copy commands from there and use them in the command line without running ccmake.

<center>
{| class="transparent" style="text-align: center"
 |valign=top|[[Image:Cmake-gui.jpg|thumb|300px|center|CMake GUI]]
 |valign=top|[[Image:Ccmake.jpg|thumb|300px|center|ccmake text interface]]
 |}
</center>

== Reproducible Builds ==

Blender's release builds use the option <tt>WITH_BUILDINFO</tt>

This includes build date and time, which means each build will be slightly different from the previous.

Typically you can just disable the <tt>WITH_BUILDINFO</tt> option, if you don't want this,
however projects may want to be able to create reproducible builds. See: https://wiki.debian.org/
To support this we have 2 optional CMake variables:

* <tt>BUILDINFO_OVERRIDE_DATE</tt>, formatted <tt>YYYY-MM-DD</tt> (year:month:day). eg "2016-07-11"
* <tt>BUILDINFO_OVERRIDE_TIME</tt>, formatted <tt>HH:MM:SS</tt> (hour:min:seconds). eg "23:54:05"

When defined, these will override the current date and time.