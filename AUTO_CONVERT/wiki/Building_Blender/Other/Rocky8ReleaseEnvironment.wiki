= Rocky 8 Release Build Environment =

The official Blender builds are done on the Rocky 8 system. This makes it possible to have a portable build which will work on [https://vfxplatform.com/ VFX reference platform] compatible systems.

This page describes how the official setup is created. This setup allows to compile Blender itself, as well as the Blender dependencies.

== Preparing Rocky 8 Environment ==

The release environment is based on the `Minimal Install` of the Rocky 8. This document does not cover the steps needed to get the OS installed: it is rather straightforward and the default installation will get to where the systems needs to be.

The [https://projects.blender.org/blender/blender/src/branch/main/build_files/build_environment/linux/linux_rocky8_setup.sh linux_rocky8_setup.sh] script is then run as a root on the freshly installed system. It takes care of installing all dependencies.

{|class="note"
|-
|<div class="note_title">'''Note'''</div>
<div class="note_content">Some system-wide changes are performed, so it is recommended to use a dedicated virtual machine for the release build environment. </div>
|}

Congratulations, the base system is now ready!

Follow the [[Building_Blender/GPU_Binaries|Building Cycles with GPU Binaries]] for the instructions related to installing the GPU compute toolkits.

== Building Blender ==

Blender is built from sources using the precompiled Rocky 8 libraries. Follow the Download Sources and Download Libraries sections from the [[Building_Blender/Linux/Generic_Distro|Linux Quick Setup]]. Note that all packages have already been installed, so that step from the Quick Setup should be skipped. Also make sure to get precompiled libraries for Rocky 8.

The build is to be performed using gcc-11 toolchain. It is activated using the `scl` command:

<source lang="bash">
scl enable gcc-toolset-11 bash
cd ~/blender-git/blender
make release
</source>