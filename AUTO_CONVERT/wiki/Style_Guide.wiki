=Style Guide=


<div class="bd-lead">

Here you can find a collection of conventions for various development activities.

</div>

* [[Style_Guide/C_Cpp|C and C++ Style Guide]]
* [[Style_Guide/Best_Practice_C_Cpp|C and C++ Best Practice]]
* [[Style_Guide/Python|Python Style Guide]]
* [[Style_Guide/GLSL|GLSL Style Guide]]
* [[Style_Guide/Commit_Messages|Commit Message Guidelines]]
* [[Reference/Release_Notes/Writing_Style|Release Notes Writing Style]]

See also:
* [[Style Guide/IDE Configuration|IDE Configuration]]
* [[Style_Guide/Code_Quality_Day|Code Quality Day]]

