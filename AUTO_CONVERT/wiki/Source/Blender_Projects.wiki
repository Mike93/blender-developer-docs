= Blender Projects =

{|class="note"
|-
|<div class="note_title">'''Note'''</div>
<div class="note_content">Describes in development work, this feature is not in the main branch.</div>
|}


While .blend files are a powerful storage format, they often do not provide sufficient context for projects that go beyond a single .blend file. In practice, projects often consist of many .blend files and all kinds of other resources. 

''Blender Projects'' were introduced to make Blender aware of this broader context, so that Blender knows that these files are just building blocks. In return Blender can optimize functionality for a multi-file project workflow, and provide features to better organize work for a given project. For example with project specific asset libraries, variables, render settings, role based editing rights, ... 

At the current stage project support is ''very'' basic, but it is a nicely scalable concept so expect more features to be added over time.

== Design Basics ==

In essence, a Blender project is a directory on a file system, including any direct and indirect contents of this directory. This directory is considered to be the ''project root directory''. It must contain a `.blender_project` sub-directory, so Blender is able to recognize it as such. This is the place where project settings and other project specific data can be stored in, such as caches, add-ons, presets, ...

When editing .blend files that are stored within a project root directory (directly or indirectly), Blender loads the project configuration into an active project.

=== The ''Active Project'' ===

When editing a .blend file that is stored inside of a project (either directly or indirectly stored in the project root directory), Blender considers this to be the ''active project''. Blender figures out which project to load and activate by "walking up" the path of the currently edited Blend file. For example if the file is stored at the path `C:\blabla\foo\bar\this-is-my-file.blend`, Blender tries to find a `.blender_project` directory first in the parent directory `bar`, then in `foo`, then `blabla`, then `C:`. Let's say this directory was found inside the `foo` directory, then Blender will load the project configuration from there as active project and `C:\blabla\foo` will be the project root directory.

In turn, if a .blend file is not stored inside of a project, or even not saved at all, there is no active project. Blender Project related features (such as the project settings editor) are not available then.

Whenever the location of the currently edited .blend file changes, Blender needs to check if it has to activate a different project. This can happen both when opening a different file, or when saving a file (which may change the file location).

=== Basic File Structure ===

<source lang="py">
/some-path/my-project/                                # The project root directory; name is arbitrary. Must contain a `.blender_project` directory.
/some-path/my-project/.blender_project/               # Directory to store project specific data, such as project settings, caches, presets, ...
/some-path/my-project/.blender_project/settings.json  # Settings for the Blender project (name, description, asset libraries, ...).
/some-path/my-project/...                             # Arbitrary files and directory to be considered part of the project.
</source>


== Noteworthy Considerations ==

* Unicode file-paths: Unicode characters in paths are unfortunately still a struggle to support well on all platforms, but they definitely need to be supported. Since Blender Projects are based on the file system contents, this needs to be stable with good automated testing coverage.
* Symbolic links in the project directory: Symbolic links inside the project are followed. So a file that is a link to some other file is treated as if the source file was the targeted file. This way the project may link to files that are not stored inside the project.

== Current Feature Set ==

* Creating projects
* Custom per project asset libraries
* Project settings stored in a human-readable format (JSON)
* Supported settings:
** Project name