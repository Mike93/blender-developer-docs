= Function & Particle Nodes =

This is a project under development to implement a new node system for manipulating objects and geometry, with as first usage a new node-based particle system.

* [[Source/Nodes/EverythingNodes|Everything Nodes]]
* [[Source/Nodes/InitialFunctionsSystem|Initial Functions System]]
* [[Source/Nodes/MeshTypeRequirements|Mesh Type Requirements]]
* [[Source/Nodes/BreakModifierStack|Breaking up the Modifier Stack]]
* [[Source/Nodes/SimulationArchitectureProposal|Simulation Architecture Proposal]]
* [[Source/Nodes/SimulationFrameworkFirstSteps|Simulation Framework - First Steps]]
* [[Source/Nodes/NodeInterfaceFramework|Node Interface Framework]]
* [[Source/Nodes/ParticleSystemNodes|Representing Particle Systems with Nodes]]
* [[Source/Nodes/ParticleSystemCodeArchitecture|Particle System Code Architecture]]
* [[Source/Nodes/UpdatedParticleNodesUI|Updated Particle Nodes UI]]
* [[Source/Nodes/UpdatedParticleNodesUI2|Updated Particle Nodes UI 2]]
* [[Source/Nodes/ParticleNodesCoreConcepts|Particle Nodes Core Concepts]]
* [[Source/Nodes/UnifiedSimulationSystemProposal|Unified Simulation System Proposal]]
* [[Source/Nodes/FunctionSystem|Function System]]