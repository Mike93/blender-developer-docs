=IDs=

IDs (also known as data-blocks, especially at user level) are the main core data structure in Blender. Almost any persistent data is an ID, or belongs to an ID, even mostly UI-related data like the `WindowManager` or `Screen` ID types.

{|class="note"
|-
|<div class="note_title">'''Disclaimer'''</div>
<div class="note_content">A lot of those pages are still stubs and need to be fully written, reviewed, etc.</div>
|}


==ID Type==
* '''[[Source/Architecture/ID/ID_Type|ID, ID_Type, IDTypeInfo]]''': Basic structure of an ID, and the ID type system.

==ID Runtime Storage==
* '''[[Source/Architecture/ID/Main|Main data-base]]''': Runtime storage of persistent IDs.
* '''[[Source/Architecture/ID/Runtime|Runtime-only IDs]]''': The various kinds of temporary, runtime-only ID datablocks.
* '''[[Source/Architecture/ID/Embedded|Embedded IDs]]''': Some IDs (Collections, NodeTrees) can be 'embedded' into other IDs.

==ID Management==
* '''[[Source/Architecture/ID/Management|ID Management]]''': Basics over ID creation, copying/duplication, and freeing. 
* '''[[Source/Architecture/ID/Management/Relationships|Relationships Between IDs]]''': Relationships between different IDs, usage refcounting, remapping...
* '''[[Source/Architecture/ID/Management/File_Paths|File Paths]]''': File paths handling in IDs.

==Related Topics==
''TODO: Also needs links to related topics: read/write, link/append, liboverride, animation, DNA, RNA, etc.''