= Anonymous Attributes =

Anonymous attributes are attributes that don’t have a name (that’s visible to the user). This has a couple of benefits: 
* Users don’t have to specify the name to use an attribute created by a node. 
* Attribute name collisions can be avoided automatically. 
* It’s possible to detect where they are used and their creation and deletion can therefore be automated. Note that this is not generally possible with named attributes, because they may even be used after export in other software. Named attributes are expected to stay around when they are not removed explicitly.

[http://developer.blender.org/D16858 D16858]: Geometry Nodes: Deterministic anonymous attribute lifetimes.