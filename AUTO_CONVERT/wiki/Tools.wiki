= Development Tools =

== Services ==

* [[Tools/Pull_Requests|Pull Requests]]: code contribution and review
* [[Tools/BuildBot|BuildBot]]: automated builds at [http://builder.blender.org builder.blender.org]
* [[Tools/Git|Git]]: how to download, update and commit code
* [[Tools/Subversion|Subversion]]: how to check out prebuild libraries and tests
* [[Tools/tea|tea]]: command line tool for interacting with Gitea

There is an [https://projects.blender.org/infrastructure/blender-projects-platform/issues issue tracker] for bugs and plans in development web services.

== Development Tools ==

* [[Tools/ClangFormat|ClangFormat]]: automatic code formatting following Blender style
* [[Building_Blender/Options#Setup_For_Developers|CMake for Developers]]: CMake options for development
* [[Tools/distcc|distcc]]: distributed building
* [[Tools/Unity_Builds|Unity Builds]]: reduce compile times
* [[Tools/Doxygen|C/C++ Code Documentation]]: viewing and editing with doxygen

== Testing ==

* [[Tools/Tests/Setup|Setup]]: setup to run tests
* [[Tools/Tests/GTest|C/C++ Tests]]: GTest tests
* [[Tools/Tests/Python|Python Tests]]: Python testing
* [[Tools/Tests/Performance|Performance Tests]]: benchmarking

== Debugging ==

=== C/C++ ===
* [[Tools/Debugging/GDB|GDB]]: debugging on Unix system
* [[Tools/Debugging/ASAN_Address_Sanitizer|Address Sanitizer]]: debugging with GCC/Clang & ASAN
* [[Tools/Debugging/Valgrind|Valgrind]]: detecting memory errors (linux/osx only)
* [[Tools/Debugging/BuGLe|BuGLe]]: OpenGL debugging
* [[Tools/Debugging/PyFromC|Py from C]]: run Python scripts in C code (test changes without rebuilds).

=== Python ===
* [[Tools/Debugging/Python_Eclipse|Eclipse PyDev]]: How to debug Python scripts running in Blender from the Eclipse IDE.
* [[Tools/Debugging/Python_Profile|Profiling]]: How to profile Python function calls.
* [[Tools/Debugging/Python_Trace|Tracing]]: How to log script execution.

== Guides ==

* [[Tools/Tips_for_Coding_Blender|Coding Tips]]: how to navigate the code and debug problems.
* [[Tools/Blender_Tools_Repo|Blender Dev Tools]]: how to setup Blenders optional developer tools repo.
* [[Tools/GitBisectWithEventSimulation|Git Bisect with Event Simulation]]: automatically find faulty commits