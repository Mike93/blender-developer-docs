= Blender Human Interface Guidelines =
 
The Blender Human Interface Guidelines (HIG) aim to provide a clear path towards understanding Blender's user interaction schemes and how they apply to its variety of workflows.<br/>
They should be a practical resource for contributors, including Add-on authors, designers and core developers.


Please respect that the HIG is rather extensive and that we can't reasonably expect contributors to know all of it.

{|class="note"
|-
|<div class="note_title">'''Work in Progress'''</div>
<div class="note_content">The Blender Human Interface Guidelines are still in development. They are unpolished and a lot of content is missing still.</div>
|}


'''Subpages''':
[[:Special:PrefixIndex/Human Interface Guidelines/]]
{|class="note"
|-
|<div class="note_title">'''Proposal for restructuring'''</div>
<div class="note_content">The current page structure is temporary, see [http://developer.blender.org/T97989 T97989].</div>
|}


== TODO ==

Here are the things we should address before sharing the HIG more widely:
* Initial vision statement.
* Add more contents to [[Human_Interface_Guidelines/Layouts|Layouts]] page.
* Add/improve contents on [[Human_Interface_Guidelines/Process|Process]] page.
* Update [[Human_Interface_Guidelines/Sidebar_Tabs|Sidebar Tabs]] for 2.8 changes.
* Clearly mark which pages are "approved".
* Make things prettier :) (mostly through pictures).