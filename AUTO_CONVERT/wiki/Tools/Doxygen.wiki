= C/C++ Source Code Docs =

Blender uses [http://www.doxygen.nl doxygen] for C/C++ source code docs, you will need to install this first.

== Building ==

We've included a convenience build target.

<source lang="bash">
make doc_doxy
</source>

This is just a convenient way to run:

<source lang="bash">
cd doc/doxygen
doxygen Doxyfile
</source>

=== Single File Builds ===

The complete documentation can take a while to build, to build docs for a single file, we have included a utility.

While this misses cross-file references, it's useful for a quick preview.

Example use:

<source lang="bash">
./source/tools/utils_doc/doxygen_single_file --browse doc/doxygen/Doxyfile /path/to/source.c
</source>

Where "/path/to/source.c" is the source file you're previewing. <tt>--browse</tt> will open up the documentation in a web browser and can be omitted if you prefer to do this manually.