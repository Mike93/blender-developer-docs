= Sprint 4 =

* '''Start''': 2020/11/30
* '''End''': 2020/12/11
* '''Goal''': {high level sentence about the goal of this sprint}.

== Time ==

* Jacques: 6 days
* Pablo: 4 days
* Hans: 6 days
* Simon: 2 days
* Sebastian: 3 days

== Promises ==

'''Nodes + Improvements
* T82765: Support for exposing object socket to the modifier
* T83108: Stable point random attribute for pebbles use case
* T83224: Nodes required to make the "random attributes" to produce stable outputs for the scattering
* T83059: Point Separate Node for flowers and trees
* T83057: Attribute Math: Greater Than for the trees and flowers
* T83225: Revise whether "attribute math" comparison modes should be their own node, for flowers and trees
* T82374: Mix Attributes Node for the trees and flowers use case

* T83226: Revise and sign off on the modifier name
* T82372: Point Instancer Node to instance Collections for the trees and flowers use case - needs updated mockup
* T82584: Sample Texture Node for trees and flowers scattering
* T82585: Attribute Color Ramp for trees and flowers scenarios
* T83237: Add error messages to nodes

'''Poisson Disk'''
* T83227: Tile baking for the poisson disk distribution for scattering
* T83228: Dithering support for the disk poisson point scattering node
* T83229: Jittering control for the disk poisson point scattering node
* T83230: Tile distribution for the disk poisson for scattering

'''Point cloud'''
* T83231: Select point cloud in edit mode for the tree leaves
* T83235: See point cloud instanced objects in edit mode for tree leaves
* T83234: Remove point cloud points in edit mode for tree leaves
* T83233: Add new point cloud points in edit mode for tree leaves
* T83232: Translate point cloud points in edit mode for tree leaves

'''Sample Files'''
* T83238: Tree leaves sample file
* T82365: Sample file for trees and flowers

'''Design'''
* T82697: "Chansey" node organization design for procedural modeling
* T82876: Find a solution for viewport / render quality
* T83239: Find a solution to preview of a part of the nodetree

'''Other'''
* T82702: Q&A development design in wiki

== Impediments ==

* Merging wasn't with issues. Dalai will take over for now.
*

== Sprint Review ==

=== What went well ===

=== What didn't go well ===

=== Improvements ===