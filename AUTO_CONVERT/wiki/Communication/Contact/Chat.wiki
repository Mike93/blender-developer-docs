= Chat =

[https://blender.chat blender.chat] is the platform where many Blender developers and users hang out. See the [https://blender.chat/directory channel listing] for all available channels. The most popular channels are listed below.

Previous mostly activity was on IRC, and a few channels are still active on [https://libera.chat/ libera.chat].

== Channels ==

=== Developers ===

'''[https://blender.chat/channel/blender-coders #blender-coders]''': Official Blender development chat room for discussions about Blender core development, where most Blender developers hang out.

'''[https://blender.chat/channel/docs #docs]''': Official Blender documentation chat room where discussions about the Blender manual.

'''[https://blender.chat/channel/python #python]''': If you need help regarding Blender's Python functionality, this is the place to visit.

'''[https://blender.chat/channel/blender-builds #blender-builds]''': Need help compiling the Blender source code? Although it's quick and easy for some people, not everyone is as lucky getting Blender to build on their platform. Drop by if you need assistance.

==== Modules ====

'''[https://blender.chat/channel/animation-module #animation-module]''': Animation development

'''[https://blender.chat/channel/blender-triagers #blender-triagers]''': Bug triaging team coordination

'''[https://blender.chat/channel/core-module #core-module]''': Core module development

'''[https://blender.chat/channel/data-assets-io-module #data-assets-io-module]''': Data, Assets & IO development

'''[https://blender.chat/channel/eevee-viewport-module #eevee-viewport-module]''': EEVEE & Viewport development

'''[https://blender.chat/channel/geometry-nodes #geometry-nodes]''': Geometry Nodes development

'''[https://blender.chat/channel/grease-pencil-module #grease-pencil-module]''': Grease Pencil development

'''[https://blender.chat/channel/render-cycles-module #render-cycles-module]''': Render & Cycles development

'''[https://blender.chat/channel/nodes-physics-module #nodes-physics-module]''': Nodes & Physics development

'''[https://blender.chat/channel/sculpt-paint-texture-module #sculpt-paint-texture-module]''': Sculpt, Paint and Texture development

'''[https://blender.chat/channel/user-interface-module #user-interface-module]''': User Interface development

=== Artists and Users ===

'''[https://blender.chat/channel/support #support]''': General support channel for those seeking help and advice with Blender, in a family friendly moderated environment.

'''[https://blender.chat/channel/general #general]''': Where Blenderheads meet and chat, ask questions, or just have fun! 

'''[https://blender.chat/channel/vse #vse]''': Support channel for discussions about the video sequence editor, rendering, encoding, processing, color spaces and editing in general.

'''[https://blender.chat/channel/gameengines-armoury-godot-up-bge-panda-etc #gameengines-armoury-godot-up-bge-panda-etc]''': Blender no longer has an integrated game engine. But here you can find discussions about the game engines, and their integrations.

=== International ===

'''[https://blender.chat/channel/aidez-moi #aidez-moi]''' and '''[https://blender.chat/channel/blender-fr #blender-fr]''': Blender chat in French

'''[https://blender.chat/channel/blender-heute #blender-heute]''': Blender chat in German

'''[https://blender.chat/channel/vandaag #vandaag]''': Blender chat in Dutch

'''[https://blender.chat/channel/hoy #hoy]''': Blender chat in Spanish

=== Support ===

For issues on blender.chat please go to the '''[https://blender.chat/channel/blender-chat-meta #blender-chat-meta]''' channel.

== IRC ==

While most activity has moved to blender.chat, some channels remain active. Previously freenode.net was used. Like much of the open-source community, activity has moved to libera.chat.

Note that the Blender Foundation no longer has an official presence on IRC, channels here are community operated.

=== Channels ===

'''[irc://irc.libera.chat/blender #blender]''': General support channel for those seeking help and advice with Blender, in a family-friendly moderated environment.

'''[irc://irc.libera.chat/blendercoders #blendercoders]''': Development discussion channel.

=== Using IRC ===

Many IRC clients are available. The easiest way to get started is to use the [https://web.libera.chat/ libera.chat web client].